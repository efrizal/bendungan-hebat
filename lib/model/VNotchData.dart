import 'package:meta/meta.dart';
import 'dart:convert';

List<DataVNotch> dataVNotchFromJson(String str) =>
    List<DataVNotch>.from(json.decode(str).map((x) => DataVNotch.fromJson(x)));

String dataVNotchToJson(List<DataVNotch> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DataVNotch {
  DataVNotch({
    @required this.id,
    @required this.deviceTypeId,
    @required this.userId,
    @required this.petugas,
    @required this.perangkatId,
    @required this.namaPerangkat,
    @required this.tanggal,
    @required this.nilaiPengukuran,
    @required this.nilaiKonstanta,
    @required this.nilaiPengolahan,
    @required this.vnotchType,
    @required this.foto,
    @required this.keterangan,
    @required this.createdAt,
    @required this.updatedAt,
    @required this.deletedAt,
  });

  String? id;
  int? deviceTypeId;
  int? userId;
  String? petugas;
  int? perangkatId;
  String? namaPerangkat;
  String? tanggal;
  String? nilaiPengukuran;
  double? nilaiKonstanta;
  double? nilaiPengolahan;
  String? vnotchType;
  dynamic? foto;
  dynamic? keterangan;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic? deletedAt;

  factory DataVNotch.fromJson(Map<String, dynamic> json) => DataVNotch(
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        petugas: json["petugas"] == null ? null : json["petugas"],
        perangkatId: json["perangkat_id"] == null ? null : json["perangkat_id"],
        namaPerangkat:
            json["nama_perangkat"] == null ? null : json["nama_perangkat"],
        tanggal: json["tanggal"] == null ? null : json["tanggal"],
        nilaiPengukuran:
            json["nilai_pengukuran"] == null ? null : json["nilai_pengukuran"],
        nilaiKonstanta: json["nilai_konstanta"] == null
            ? 0.0
            : json["nilai_konstanta"].toDouble(),
        nilaiPengolahan: json["nilai_pengolahan"] == null
            ? null
            : json["nilai_pengolahan"].toDouble(),
        vnotchType: json["vnotch_type"] == null ? null : json["vnotch_type"],
        foto: json["foto"],
        keterangan: json["keterangan"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "user_id": userId == null ? null : userId,
        "petugas": petugas == null ? null : petugas,
        "perangkat_id": perangkatId == null ? null : perangkatId,
        "nama_perangkat": namaPerangkat == null ? null : namaPerangkat,
        "tanggal": tanggal == null ? null : tanggal,
        "nilai_pengukuran": nilaiPengukuran == null ? null : nilaiPengukuran,
        "nilai_konstanta": nilaiKonstanta == null ? null : nilaiKonstanta,
        "nilai_pengolahan": nilaiPengolahan == null ? null : nilaiPengolahan,
        "vnotch_type": vnotchType == null ? null : vnotchType,
        "foto": foto,
        "keterangan": keterangan,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

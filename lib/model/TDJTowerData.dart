// To parse this JSON data, do
//
//     final Data3DT = Data3DTFromJson(jsonString);

import 'dart:convert';

List<Data3DT> Data3DTFromJson(String str) =>
    List<Data3DT>.from(json.decode(str).map((x) => Data3DT.fromJson(x)));

String Data3DTToJson(List<Data3DT> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Data3DT {
  Data3DT({
    this.id,
    this.deviceTypeId,
    this.userId,
    this.petugas,
    this.perangkatId,
    this.namaPerangkat,
    this.tanggal,
    this.nilaiPengukuranA,
    this.nilaiPengukuranB,
    this.nilaiPengukuranC,
    this.nilaiKonstantaA,
    this.nilaiKonstantaB,
    this.nilaiKonstantaC,
    this.nilaiPengolahanA,
    this.nilaiPengolahanB,
    this.nilaiPengolahanC,
    this.foto,
    this.keterangan,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  String? id;
  int? deviceTypeId;
  int? userId;
  String? petugas;
  int? perangkatId;
  String? namaPerangkat;
  String? tanggal;
  double? nilaiPengukuranA;
  double? nilaiPengukuranB;
  double? nilaiPengukuranC;
  double? nilaiKonstantaA;
  double? nilaiKonstantaB;
  double? nilaiKonstantaC;
  double? nilaiPengolahanA;
  double? nilaiPengolahanB;
  double? nilaiPengolahanC;
  String? foto;
  String? keterangan;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic? deletedAt;

  factory Data3DT.fromJson(Map<String, dynamic> json) => Data3DT(
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        petugas: json["petugas"] == null ? null : json["petugas"],
        perangkatId: json["perangkat_id"] == null ? null : json["perangkat_id"],
        namaPerangkat:
            json["nama_perangkat"] == null ? null : json["nama_perangkat"],
        tanggal: json["tanggal"] == null ? null : json["tanggal"],
        nilaiPengukuranA: json["nilai_pengukuran_a"] == null
            ? null
            : json["nilai_pengukuran_a"].toDouble(),
        nilaiPengukuranB: json["nilai_pengukuran_b"] == null
            ? null
            : json["nilai_pengukuran_b"].toDouble(),
        nilaiPengukuranC: json["nilai_pengukuran_c"] == null
            ? null
            : json["nilai_pengukuran_c"].toDouble(),
        nilaiKonstantaA: json["nilai_konstanta_a"] == null
            ? null
            : json["nilai_konstanta_a"].toDouble(),
        nilaiKonstantaB: json["nilai_konstanta_b"] == null
            ? null
            : json["nilai_konstanta_b"].toDouble(),
        nilaiKonstantaC: json["nilai_konstanta_c"] == null
            ? null
            : json["nilai_konstanta_c"].toDouble(),
        nilaiPengolahanA: json["nilai_pengolahan_a"] == null
            ? null
            : json["nilai_pengolahan_a"].toDouble(),
        nilaiPengolahanB: json["nilai_pengolahan_b"] == null
            ? null
            : json["nilai_pengolahan_b"].toDouble(),
        nilaiPengolahanC: json["nilai_pengolahan_c"] == null
            ? null
            : json["nilai_pengolahan_c"].toDouble(),
        foto: json["foto"] == null ? null : json["foto"],
        keterangan: json["keterangan"] == null ? null : json["keterangan"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "user_id": userId == null ? null : userId,
        "petugas": petugas == null ? null : petugas,
        "perangkat_id": perangkatId == null ? null : perangkatId,
        "nama_perangkat": namaPerangkat == null ? null : namaPerangkat,
        "tanggal": tanggal == null ? null : tanggal,
        "nilai_pengukuran_a":
            nilaiPengukuranA == null ? null : nilaiPengukuranA,
        "nilai_pengukuran_b":
            nilaiPengukuranB == null ? null : nilaiPengukuranB,
        "nilai_pengukuran_c":
            nilaiPengukuranC == null ? null : nilaiPengukuranC,
        "nilai_konstanta_a": nilaiKonstantaA == null ? null : nilaiKonstantaA,
        "nilai_konstanta_b": nilaiKonstantaB == null ? null : nilaiKonstantaB,
        "nilai_konstanta_c": nilaiKonstantaC == null ? null : nilaiKonstantaC,
        "nilai_pengolahan_a":
            nilaiPengolahanA == null ? null : nilaiPengolahanA,
        "nilai_pengolahan_b":
            nilaiPengolahanB == null ? null : nilaiPengolahanB,
        "nilai_pengolahan_c":
            nilaiPengolahanC == null ? null : nilaiPengolahanC,
        "foto": foto == null ? null : foto,
        "keterangan": keterangan == null ? null : keterangan,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

// To parse this JSON data, do
//
//     final dataIch = dataIchFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<DataIch> dataIchFromJson(String str) =>
    List<DataIch>.from(json.decode(str).map((x) => DataIch.fromJson(x)));

String dataIchToJson(List<DataIch> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DataIch {
  DataIch({
    this.id,
    this.deviceTypeId,
    this.userId,
    this.petugas,
    this.perangkatId,
    this.namaPerangkat,
    this.tanggal,
    this.nilaiSt1,
    this.nilaiSt2,
    this.nilaiSt3,
    this.foto,
    this.keterangan,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  String? id;
  int? deviceTypeId;
  int? userId;
  String? petugas;
  int? perangkatId;
  String? namaPerangkat;
  String? tanggal;
  String? nilaiSt1;
  String? nilaiSt2;
  String? nilaiSt3;
  dynamic? foto;
  dynamic? keterangan;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic? deletedAt;

  factory DataIch.fromJson(Map<String, dynamic> json) => DataIch(
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        petugas: json["petugas"] == null ? null : json["petugas"],
        perangkatId: json["perangkat_id"] == null ? null : json["perangkat_id"],
        namaPerangkat:
            json["nama_perangkat"] == null ? null : json["nama_perangkat"],
        tanggal: json["tanggal"] == null ? null : json["tanggal"],
        nilaiSt1: json["nilai_st1"] == null ? "0.0" : json["nilai_st1"],
        nilaiSt2: json["nilai_st2"] == null ? "0.0" : json["nilai_st2"],
        nilaiSt3: json["nilai_st3"] == null ? "0.0" : json["nilai_st3"],
        foto: json["foto"],
        keterangan: json["keterangan"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "user_id": userId == null ? null : userId,
        "petugas": petugas == null ? null : petugas,
        "perangkat_id": perangkatId == null ? null : perangkatId,
        "nama_perangkat": namaPerangkat == null ? null : namaPerangkat,
        "tanggal": tanggal == null ? null : tanggal,
        "nilai_st1": nilaiSt1 == null ? null : nilaiSt1,
        "nilai_st2": nilaiSt2 == null ? null : nilaiSt2,
        "nilai_st3": nilaiSt3 == null ? null : nilaiSt3,
        "foto": foto,
        "keterangan": keterangan,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

// To parse this JSON data, do
//
//     final userData = userDataFromJson(jsonString);

import 'dart:convert';

UserData userDataFromJson(String str) => UserData.fromJson(json.decode(str));

String userDataToJson(UserData data) => json.encode(data.toJson());

class UserData {
  UserData({
    this.id,
    this.name,
    this.email,
    this.password,
    this.avatar,
    this.status,
    this.ktp,
    this.noHandphone,
    this.alamat,
    this.gender,
    this.level,
    this.deletedAt,
    this.updatedAt,
    this.createdAt,
  });

  String? id;
  String? name;
  String? email;
  String? password;
  dynamic avatar;
  int? status;
  String? ktp;
  String? noHandphone;
  String? alamat;
  String? gender;
  int? level;
  dynamic deletedAt;
  DateTime? updatedAt;
  DateTime? createdAt;

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        password: json["password"] == null ? null : json["password"],
        avatar: json["avatar"] == null
            ? "https://drive.google.com/uc?export=download&id=1CFB9jE3MVZRgq4Be5jM4vNtBIJ5dPzHg"
            : json["avatar"],
        status: json["status"] == null ? null : json["status"],
        ktp: json["ktp"] == null ? null : json["ktp"],
        noHandphone: json["no_handphone"] == null ? null : json["no_handphone"],
        alamat: json["alamat"] == null ? null : json["alamat"],
        gender: json["gender"] == null ? null : json["gender"],
        level: json["level"] == null ? null : json["level"],
        deletedAt: json["deleted_at"],
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "password": password == null ? null : password,
        "avatar": avatar,
        "status": status == null ? null : status,
        "ktp": ktp == null ? null : ktp,
        "no_handphone": noHandphone == null ? null : noHandphone,
        "alamat": alamat == null ? null : alamat,
        "gender": gender == null ? null : gender,
        "level": level == null ? null : level,
        "deleted_at": deletedAt,
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
      };
}

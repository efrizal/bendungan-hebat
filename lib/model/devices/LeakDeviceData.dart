// To parse this JSON data, do
//
//     final dataLeakageDevices = dataLeakageDevicesFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

DataLeakageDevices dataLeakageDevicesFromJson(String str) =>
    DataLeakageDevices.fromJson(json.decode(str));

String dataLeakageDevicesToJson(DataLeakageDevices data) =>
    json.encode(data.toJson());

class DataLeakageDevices {
  DataLeakageDevices({
    this.name,
    this.id,
    this.deviceTypeId,
    this.deviceTypeName,
    this.deviceSatuan,
    this.nilaiKonstanta,
    this.status,
    this.createdAt,
    this.deletedAt,
  });

  String? name;
  int? id;
  int? deviceTypeId;
  String? deviceTypeName;
  dynamic? deviceSatuan;
  dynamic? nilaiKonstanta;
  int? status;
  DateTime? createdAt;
  dynamic? deletedAt;

  factory DataLeakageDevices.fromJson(Map<String, dynamic> json) =>
      DataLeakageDevices(
        name: json["name"] == null ? null : json["name"],
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        deviceTypeName:
            json["device_type_name"] == null ? null : json["device_type_name"],
        deviceSatuan: json["device_satuan"],
        nilaiKonstanta: json["nilai_konstanta"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "device_type_name": deviceTypeName == null ? null : deviceTypeName,
        "device_satuan": deviceSatuan,
        "nilai_konstanta": nilaiKonstanta,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

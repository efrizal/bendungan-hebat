// To parse this JSON data, do
//
//     final dataOwDevices = dataOwDevicesFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

DataOwDevices dataOwDevicesFromJson(String str) =>
    DataOwDevices.fromJson(json.decode(str));

String dataOwDevicesToJson(DataOwDevices data) => json.encode(data.toJson());

class DataOwDevices {
  DataOwDevices({
    this.name,
    this.id,
    this.deviceTypeId,
    this.deviceTypeName,
    this.deviceSatuan,
    this.nilaiKonstanta,
    this.status,
    this.createdAt,
    this.deletedAt,
  });

  String? name;
  int? id;
  int? deviceTypeId;
  String? deviceTypeName;
  dynamic? deviceSatuan;
  double? nilaiKonstanta;
  int? status;
  DateTime? createdAt;
  dynamic? deletedAt;

  factory DataOwDevices.fromJson(Map<String, dynamic> json) => DataOwDevices(
        name: json["name"] == null ? null : json["name"],
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        deviceTypeName:
            json["device_type_name"] == null ? null : json["device_type_name"],
        deviceSatuan: json["device_satuan"],
        nilaiKonstanta: json["nilai_konstanta"] == null
            ? null
            : json["nilai_konstanta"].toDouble(),
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "device_type_name": deviceTypeName == null ? null : deviceTypeName,
        "device_satuan": deviceSatuan,
        "nilai_konstanta": nilaiKonstanta == null ? null : nilaiKonstanta,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

// To parse this JSON data, do
//
//     final DataPiezoDevices = DataPiezoDevicesFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<DataPiezoDevices> dataPiezoDevicesFromJson(String str) =>
    List<DataPiezoDevices>.from(
        json.decode(str).map((x) => DataPiezoDevices.fromJson(x)));

String dataPiezoDevicesToJson(List<DataPiezoDevices> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DataPiezoDevices {
  DataPiezoDevices({
    this.name,
    this.id,
    this.deviceTypeId,
    this.deviceTypeName,
    this.deviceSatuan,
    this.nilaiKonstanta1,
    this.nilaiKonstanta2,
    this.nilaiOperator,
    this.status,
    this.createdAt,
    this.deletedAt,
  });

  String? name;
  int? id;
  int? deviceTypeId;
  String? deviceTypeName;
  String? deviceSatuan;
  double? nilaiKonstanta1;
  double? nilaiKonstanta2;
  String? nilaiOperator;
  int? status;
  DateTime? createdAt;
  dynamic? deletedAt;

  factory DataPiezoDevices.fromJson(Map<String, dynamic> json) =>
      DataPiezoDevices(
        name: json["name"] == null ? null : json["name"],
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        deviceTypeName:
            json["device_type_name"] == null ? null : json["device_type_name"],
        deviceSatuan:
            json["device_satuan"] == null ? null : json["device_satuan"],
        nilaiKonstanta1: json["nilai_konstanta_1"] == null
            ? null
            : json["nilai_konstanta_1"].toDouble(),
        nilaiKonstanta2: json["nilai_konstanta_2"] == null
            ? null
            : json["nilai_konstanta_2"].toDouble(),
        nilaiOperator:
            json["nilai_operator"] == null ? null : json["nilai_operator"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "device_type_name": deviceTypeName == null ? null : deviceTypeName,
        "device_satuan": deviceSatuan == null ? null : deviceSatuan,
        "nilai_konstanta_1": nilaiKonstanta1 == null ? null : nilaiKonstanta1,
        "nilai_konstanta_2": nilaiKonstanta2 == null ? null : nilaiKonstanta2,
        "nilai_operator": nilaiOperator == null ? null : nilaiOperator,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

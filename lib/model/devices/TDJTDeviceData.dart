// To parse this JSON data, do
//
//     final dataTdjtDevices = dataTdjtDevicesFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<DataTdjtDevices> dataTdjtDevicesFromJson(String str) =>
    List<DataTdjtDevices>.from(
        json.decode(str).map((x) => DataTdjtDevices.fromJson(x)));

String dataTdjtDevicesToJson(List<DataTdjtDevices> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DataTdjtDevices {
  DataTdjtDevices({
    this.name,
    this.id,
    this.deviceTypeId,
    this.deviceTypeName,
    this.deviceSatuan,
    this.nilaiKonstantaA,
    this.nilaiKonstantaB,
    this.nilaiKonstantaC,
    this.status,
    this.createdAt,
    this.deletedAt,
  });

  String? name;
  int? id;
  int? deviceTypeId;
  String? deviceTypeName;
  dynamic? deviceSatuan;
  double? nilaiKonstantaA;
  double? nilaiKonstantaB;
  double? nilaiKonstantaC;
  int? status;
  DateTime? createdAt;
  dynamic? deletedAt;

  factory DataTdjtDevices.fromJson(Map<String, dynamic> json) =>
      DataTdjtDevices(
        name: json["name"] == null ? null : json["name"],
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        deviceTypeName:
            json["device_type_name"] == null ? null : json["device_type_name"],
        deviceSatuan: json["device_satuan"],
        nilaiKonstantaA: json["nilai_konstanta_a"] == null
            ? null
            : json["nilai_konstanta_a"].toDouble(),
        nilaiKonstantaB: json["nilai_konstanta_b"] == null
            ? null
            : json["nilai_konstanta_b"].toDouble(),
        nilaiKonstantaC: json["nilai_konstanta_c"] == null
            ? null
            : json["nilai_konstanta_c"].toDouble(),
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "device_type_name": deviceTypeName == null ? null : deviceTypeName,
        "device_satuan": deviceSatuan,
        "nilai_konstanta_a": nilaiKonstantaA == null ? null : nilaiKonstantaA,
        "nilai_konstanta_b": nilaiKonstantaB == null ? null : nilaiKonstantaB,
        "nilai_konstanta_c": nilaiKonstantaC == null ? null : nilaiKonstantaC,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

// To parse this JSON data, do
//
//     final dataVNotchDevices = dataVNotchDevicesFromJson(jsonString);

import 'dart:convert';

List<DataVNotchDevices> dataVNotchDevicesFromJson(String str) =>
    List<DataVNotchDevices>.from(
        json.decode(str).map((x) => DataVNotchDevices.fromJson(x)));

String dataVNotchDevicesToJson(List<DataVNotchDevices> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DataVNotchDevices {
  DataVNotchDevices({
    this.name,
    this.id,
    this.deviceTypeId,
    this.deviceTypeName,
    this.deviceSatuan,
    this.nilaiKonstanta,
    this.vnotchType,
    this.status,
    this.createdAt,
    this.deletedAt,
  });

  String? name;
  int? id;
  int? deviceTypeId;
  String? deviceTypeName;
  dynamic? deviceSatuan;
  dynamic? nilaiKonstanta;
  String? vnotchType;
  int? status;
  DateTime? createdAt;
  dynamic? deletedAt;

  factory DataVNotchDevices.fromJson(Map<String, dynamic> json) =>
      DataVNotchDevices(
        name: json["name"] == null ? null : json["name"],
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        deviceTypeName:
            json["device_type_name"] == null ? null : json["device_type_name"],
        deviceSatuan: json["device_satuan"],
        nilaiKonstanta: json["nilai_konstanta"],
        vnotchType: json["vnotch_type"] == null ? null : json["vnotch_type"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "device_type_name": deviceTypeName == null ? null : deviceTypeName,
        "device_satuan": deviceSatuan,
        "nilai_konstanta": nilaiKonstanta,
        "vnotch_type": vnotchType == null ? null : vnotchType,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

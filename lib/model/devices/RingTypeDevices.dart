// To parse this JSON data, do
//
//     final ringTypeDevices = ringTypeDevicesFromJson(jsonString);

import 'dart:convert';

RingTypeDevices ringTypeDevicesFromJson(String str) =>
    RingTypeDevices.fromJson(json.decode(str));

String ringTypeDevicesToJson(RingTypeDevices data) =>
    json.encode(data.toJson());

class RingTypeDevices {
  RingTypeDevices({
    this.ringNama,
    this.ringDepth,
    this.id,
    this.createdAt,
    this.deletedAt,
  });

  String? ringNama;
  String? ringDepth;
  int? id;
  dynamic? createdAt;
  dynamic? deletedAt;

  factory RingTypeDevices.fromJson(Map<String, dynamic> json) =>
      RingTypeDevices(
        ringNama: json["ring_nama"] == null ? null : json["ring_nama"],
        ringDepth: json["ring_depth"] == null ? null : json["ring_depth"],
        id: json["id"] == null ? null : json["id"],
        createdAt: json["created_at"],
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "ring_nama": ringNama == null ? null : ringNama,
        "ring_depth": ringDepth == null ? null : ringDepth,
        "id": id == null ? null : id,
        "created_at": createdAt,
        "deleted_at": deletedAt,
      };
}

// To parse this JSON data, do
//
//     final dataRingMagnet = dataRingMagnetFromJson(jsonString);

import 'dart:convert';

List<DataRingMagnet> dataRingMagnetFromJson(String str) =>
    List<DataRingMagnet>.from(
        json.decode(str).map((x) => DataRingMagnet.fromJson(x)));

String dataRingMagnetToJson(List<DataRingMagnet> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DataRingMagnet {
  DataRingMagnet(
      {this.id,
      this.userId,
      this.petugas,
      this.ringNama,
      this.nilaiElevasi,
      this.nilaiDelta,
      this.nilaiDeltaKumulatif,
      this.nilaiKedalamanVertikal,
      this.tanggal,
      this.nilaiPengukuran,
      this.keterangan,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.foto,
      this.deviceTypeId});

  int? id;
  int? userId;
  String? petugas;
  String? ringNama;
  double? nilaiElevasi;
  double? nilaiDelta;
  double? nilaiDeltaKumulatif;
  double? nilaiKedalamanVertikal;
  String? tanggal;
  String? nilaiPengukuran;
  dynamic? keterangan;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic? deletedAt;
  String? deviceTypeId;
  String? foto;

  factory DataRingMagnet.fromJson(Map<String, dynamic> json) => DataRingMagnet(
        foto: json["foto"] == null ? null : json["foto"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        petugas: json["petugas"] == null ? null : json["petugas"],
        ringNama: json["ring_nama"] == null ? null : json["ring_nama"],
        nilaiElevasi: json["nilai_elevasi"] == null
            ? null
            : json["nilai_elevasi"].toDouble(),
        nilaiDelta:
            json["nilai_delta"] == null ? null : json["nilai_delta"].toDouble(),
        nilaiDeltaKumulatif: json["nilai_delta_kumulatif"] == null
            ? null
            : json["nilai_delta_kumulatif"].toDouble(),
        nilaiKedalamanVertikal: json["nilai_kedalaman_vertikal"] == null
            ? null
            : json["nilai_kedalaman_vertikal"].toDouble(),
        tanggal: json["tanggal"] == null ? null : json["tanggal"],
        nilaiPengukuran:
            json["nilai_pengukuran"] == null ? null : json["nilai_pengukuran"],
        keterangan: json["keterangan"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "petugas": petugas == null ? null : petugas,
        "ring_nama": ringNama == null ? null : ringNama,
        "nilai_elevasi": nilaiElevasi == null ? null : nilaiElevasi,
        "nilai_delta": nilaiDelta == null ? null : nilaiDelta,
        "nilai_delta_kumulatif":
            nilaiDeltaKumulatif == null ? null : nilaiDeltaKumulatif,
        "nilai_kedalaman_vertikal":
            nilaiKedalamanVertikal == null ? null : nilaiKedalamanVertikal,
        "tanggal": tanggal == null ? null : tanggal,
        "nilai_pengukuran": nilaiPengukuran == null ? null : nilaiPengukuran,
        "keterangan": keterangan,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

// To parse this JSON data, do
//
//     final damCategoryData = damCategoryDataFromJson(jsonString);

import 'dart:convert';

List<DamCategoryData> damCategoryDataFromJson(String str) =>
    List<DamCategoryData>.from(
        json.decode(str).map((x) => DamCategoryData.fromJson(x)));

String damCategoryDataToJson(List<DamCategoryData> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DamCategoryData {
  DamCategoryData({
    this.id,
    this.type,
    this.description,
    this.icon,
    this.orderAt,
    this.createdAt,
    this.deletedAt,
  });

  int? id;
  String? type;
  String? description;
  dynamic? icon;
  int? orderAt;
  DateTime? createdAt;
  dynamic? deletedAt;

  factory DamCategoryData.fromJson(Map<String, dynamic> json) =>
      DamCategoryData(
        id: json["id"] == null ? null : json["id"],
        type: json["type"] == null ? null : json["type"],
        description: json["description"] == null ? null : json["description"],
        icon: json["icon"] == null ? null : json["icon"],
        orderAt: json["order_at"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "description": description == null ? null : description,
        "icon": icon,
        "order_at": orderAt,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

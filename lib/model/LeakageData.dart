// To parse this JSON data, do
//
//     final dataLeakage = dataLeakageFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<DataLeakage> dataLeakageFromJson(String str) => List<DataLeakage>.from(
    json.decode(str).map((x) => DataLeakage.fromJson(x)));

String dataLeakageToJson(List<DataLeakage> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DataLeakage {
  DataLeakage({
    @required this.id,
    @required this.deviceTypeId,
    @required this.userId,
    @required this.petugas,
    @required this.perangkatId,
    @required this.namaPerangkat,
    @required this.tanggal,
    @required this.nilaiVolume,
    @required this.nilaiDetik,
    @required this.nilaiPengolahan,
    @required this.foto,
    @required this.keterangan,
    @required this.createdAt,
    @required this.updatedAt,
    @required this.deletedAt,
  });

  String? id;
  int? deviceTypeId;
  int? userId;
  String? petugas;
  int? perangkatId;
  String? namaPerangkat;
  String? tanggal;
  double? nilaiVolume;
  double? nilaiDetik;
  double? nilaiPengolahan;
  String? foto;
  String? keterangan;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic? deletedAt;

  factory DataLeakage.fromJson(Map<String, dynamic> json) => DataLeakage(
        id: json["id"] == null ? null : json["id"],
        deviceTypeId:
            json["device_type_id"] == null ? null : json["device_type_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        petugas: json["petugas"] == null ? null : json["petugas"],
        perangkatId: json["perangkat_id"] == null ? null : json["perangkat_id"],
        namaPerangkat:
            json["nama_perangkat"] == null ? null : json["nama_perangkat"],
        tanggal: json["tanggal"] == null ? null : json["tanggal"],
        nilaiVolume: json["nilai_volume"] == null
            ? null
            : json["nilai_volume"].toDouble(),
        nilaiDetik:
            json["nilai_detik"] == null ? null : json["nilai_detik"].toDouble(),
        nilaiPengolahan: json["nilai_pengolahan"] == null
            ? null
            : json["nilai_pengolahan"].toDouble(),
        foto: json["foto"] == null ? null : json["foto"],
        keterangan: json["keterangan"] == null ? null : json["keterangan"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "device_type_id": deviceTypeId == null ? null : deviceTypeId,
        "user_id": userId == null ? null : userId,
        "petugas": petugas == null ? null : petugas,
        "perangkat_id": perangkatId == null ? null : perangkatId,
        "nama_perangkat": namaPerangkat == null ? null : namaPerangkat,
        "tanggal": tanggal == null ? null : tanggal,
        "nilai_volume": nilaiVolume == null ? null : nilaiVolume,
        "nilai_detik": nilaiDetik == null ? null : nilaiDetik,
        "nilai_pengolahan": nilaiPengolahan == null ? null : nilaiPengolahan,
        "foto": foto == null ? null : foto,
        "keterangan": keterangan == null ? null : keterangan,
        "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
        "deleted_at": deletedAt,
      };
}

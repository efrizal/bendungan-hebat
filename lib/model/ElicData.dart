// To parse this JSON data, do
//
//     final elicData = elicDataFromJson(jsonString);

import 'dart:convert';

ElicData elicDataFromJson(String str) => ElicData.fromJson(json.decode(str));

String elicDataToJson(ElicData data) => json.encode(data.toJson());

class ElicData {
  ElicData({
    this.id,
    this.tanggal,
    this.devicesNama,
    this.nilaiPengukuran,
  });

  int? id;
  String? tanggal;
  String? devicesNama;
  double? nilaiPengukuran;

  factory ElicData.fromJson(Map<String, dynamic> json) => ElicData(
        id: json["id"] == null ? null : json["id"],
        tanggal: json["tanggal"] == null ? null : json["tanggal"],
        devicesNama: json["devices_nama"] == null ? null : json["devices_nama"],
        nilaiPengukuran: json["nilai_pengukuran"] == null
            ? null
            : json["nilai_pengukuran"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "tanggal": tanggal == null ? null : tanggal,
        "devices_nama": devicesNama == null ? null : devicesNama,
        "nilai_pengukuran": nilaiPengukuran == null ? null : nilaiPengukuran,
      };
}

import 'package:bendungan_hebat/pages/splashscreen.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Bendungan Hebat',
      theme: ConfigTheme.primaryTheme,
      home: SplashScreen(),
      defaultTransition: Transition.fadeIn,
    );
  }
}

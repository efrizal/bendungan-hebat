import 'package:bendungan_hebat/model/DamCategoryData.dart';
import 'package:bendungan_hebat/model/ElicData.dart';
import 'package:bendungan_hebat/model/ICHData.dart';
import 'package:bendungan_hebat/model/LeakageData.dart';
import 'package:bendungan_hebat/model/OWData.dart';
import 'package:bendungan_hebat/model/PiezometerData.dart';
import 'package:bendungan_hebat/model/RingMagnetData.dart';
import 'package:bendungan_hebat/model/TDJTowerData.dart';
import 'package:bendungan_hebat/model/TMAWData.dart';
import 'package:bendungan_hebat/model/TailraceData.dart';
import 'package:bendungan_hebat/model/ThreeDimensionData.dart';
import 'package:bendungan_hebat/model/UserData.dart';
import 'package:bendungan_hebat/model/VNotchData.dart';
import 'package:bendungan_hebat/model/devices/CurahHujanDeviceData.dart';
import 'package:bendungan_hebat/model/devices/LeakDeviceData.dart';
import 'package:bendungan_hebat/model/devices/OWDeviceData.dart';
import 'package:bendungan_hebat/model/devices/PiezoDeviceData.dart';
import 'package:bendungan_hebat/model/devices/RingMagnetDeviceData.dart';
import 'package:bendungan_hebat/model/devices/RingTypeDevices.dart';
import 'package:bendungan_hebat/model/devices/TDJGDeviceData.dart';
import 'package:bendungan_hebat/model/devices/TDJTDeviceData.dart';
import 'package:bendungan_hebat/model/devices/TMATDeviceData.dart';
import 'package:bendungan_hebat/model/devices/TMAWDeviceData.dart';
import 'package:bendungan_hebat/model/devices/VNotchDeviceData.dart';
import 'package:bendungan_hebat/utils/configRestApi.dart';

import 'authServices.dart';
import 'baseServices.dart';

class DamDataServices extends BaseServices {
  // Network Util
  final ConfigRestApi networkUtil = new ConfigRestApi();

  Future<dynamic> getDamCategory() async {
    var url = "${baseUrl}devices/allType";

    try {
      var response = await networkUtil.get(url);

      final List<DamCategoryData> data = List<DamCategoryData>.from(
        response["data"].map(
          (f) => DamCategoryData.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getOWData() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/ow/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<DataOW> data = List<DataOW>.from(
        response["data"].map(
          (f) => DataOW.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getPiezoData() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/piezo/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<DataPiezometer> data = List<DataPiezometer>.from(
        response["data"].map(
          (f) => DataPiezometer.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getLeakData() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/leakage/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<DataLeakage> data = List<DataLeakage>.from(
        response["data"].map(
          (f) => DataLeakage.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> get3DGalleryData() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/tdj-access/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<Data3D> data = List<Data3D>.from(
        response["data"].map(
          (f) => Data3D.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getVNotchdata() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/vnotch/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<DataVNotch> data = List<DataVNotch>.from(
        response["data"].map(
          (f) => DataVNotch.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getRingMData() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/ring-magnet/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<DataRingMagnet> data = List<DataRingMagnet>.from(
        response["data"].map(
          (f) => DataRingMagnet.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getIntesitasCurahHujan() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/curah-hujan/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<DataIch> data = List<DataIch>.from(
        response["data"].map(
          (f) => DataIch.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getTailraceData() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/tma-tailrace/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<DataTailrace> data = List<DataTailrace>.from(
        response["data"].map(
          (f) => DataTailrace.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getTMAWData() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/tma-waduk/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<DataTmaw> data = List<DataTmaw>.from(
        response["data"].map(
          (f) => DataTmaw.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> get3DTowerData() async {
    UserData uData = await AuthServices().getCachedUserData();

    var url = "${baseUrl}nilai/tdj-tower/user/${uData.id}";

    try {
      var response = await networkUtil.get(url);

      final List<Data3DT> data = List<Data3DT>.from(
        response["data"].map(
          (f) => Data3DT.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getRingMDevices() async {
    var url = "${baseUrl}devices/ring-magnet";

    try {
      var response = await networkUtil.get(url);

      final List<DataRingMagnetDevices> data = List<DataRingMagnetDevices>.from(
        response["data"].map(
          (f) => DataRingMagnetDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getElic(String month, year, devId) async {
    var url = "${baseUrl}nilai/elic/bymonth";

    try {
      var response = await networkUtil.post(
        url,
        body: {
          "bulan": month,
          "tahun": year,
          "perangkat": devId,
        },
      );

      final List<ElicData> data = List<ElicData>.from(
        response["data"].map(
          (f) => ElicData.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getRingTypeDevices() async {
    var url = "${baseUrl}devices/ring-magnet-depth";

    try {
      var response = await networkUtil.get(url);

      final List<RingTypeDevices> data = List<RingTypeDevices>.from(
        response["data"].map(
          (f) => RingTypeDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getVNotchDevices() async {
    var url = "${baseUrl}devices/vnotch";

    try {
      var response = await networkUtil.get(url);

      final List<DataVNotchDevices> data = List<DataVNotchDevices>.from(
        response["data"].map(
          (f) => DataVNotchDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getTDJTDevices() async {
    var url = "${baseUrl}devices/tdj-tower";

    try {
      var response = await networkUtil.get(url);

      final List<DataTdjtDevices> data = List<DataTdjtDevices>.from(
        response["data"].map(
          (f) => DataTdjtDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getTDJAGDevices() async {
    var url = "${baseUrl}devices/tdj-access-gallery";

    try {
      var response = await networkUtil.get(url);

      final List<DataTdjgDevices> data = List<DataTdjgDevices>.from(
        response["data"].map(
          (f) => DataTdjgDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getPiezoDevices() async {
    var url = "${baseUrl}devices/piezometer";

    try {
      var response = await networkUtil.get(url);

      final List<DataPiezoDevices> data = List<DataPiezoDevices>.from(
        response["data"].map(
          (f) => DataPiezoDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getOWDevices() async {
    var url = "${baseUrl}devices/ow";

    try {
      var response = await networkUtil.get(url);

      final List<DataOwDevices> data = List<DataOwDevices>.from(
        response["data"].map(
          (f) => DataOwDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getLeakageDevices() async {
    var url = "${baseUrl}devices/leakage";

    try {
      var response = await networkUtil.get(url);

      final List<DataLeakageDevices> data = List<DataLeakageDevices>.from(
        response["data"].map(
          (f) => DataLeakageDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getCurahHujanDevices() async {
    var url = "${baseUrl}devices/curah-hujan";

    try {
      var response = await networkUtil.get(url);

      final List<DataCurahHujanDevices> data = List<DataCurahHujanDevices>.from(
        response["data"].map(
          (f) => DataCurahHujanDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getTMATDevices() async {
    var url = "${baseUrl}devices/tma-tailrace";

    try {
      var response = await networkUtil.get(url);

      final List<DataTailraceDevices> data = List<DataTailraceDevices>.from(
        response["data"].map(
          (f) => DataTailraceDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> getTMAWDevices() async {
    var url = "${baseUrl}devices/tma-waduk";

    try {
      var response = await networkUtil.get(url);

      final List<DataTmawDevices> data = List<DataTmawDevices>.from(
        response["data"].map(
          (f) => DataTmawDevices.fromJson(f),
        ),
      ).toList();

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> calcPiezo(calc1, calc2, calc3, calc4) async {
    var url = "${baseUrl}nilai/piezo/calc";

    try {
      var response = await networkUtil.post(url, body: {
        "nilai_pengukuran": calc1,
        "piezo_nilai_konstanta_1": calc2,
        "piezo_nilai_konstanta_2": calc3,
        "piezo_nilai_operator": calc4,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> calcRing([calc1, calc2, calc3, calc4, calc5, calc6]) async {
    var url = "${baseUrl}nilai/ring-magnet/calc";

    try {
      var response = await networkUtil.post(url, body: {
        "nilai_pengukuran": calc1,
        "nilai_konstanta": calc2,
        "nilai_elic": calc3,
        "bulan": calc4,
        "tahun": calc5,
        "ring": calc6,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> calcLeakage(calc1, calc2) async {
    var url = "${baseUrl}nilai/leakage/calc";

    try {
      var response = await networkUtil.post(url, body: {
        "nilai_volume": calc1,
        "nilai_detik": calc2,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> calcTDJT(calc1, calc2) async {
    var url = "${baseUrl}nilai/tdj-tower/calc";

    try {
      var response = await networkUtil.post(url, body: {
        "nilai_pengukuran": calc1,
        "nilai_konstanta": calc2,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> calcTDJG(calc1, calc2) async {
    var url = "${baseUrl}nilai/tdj-access/calc";

    try {
      var response = await networkUtil.post(url, body: {
        "nilai_pengukuran": calc1,
        "nilai_konstanta": calc2,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> calcVNotch(calc1, calc2, calc3) async {
    var url = "${baseUrl}nilai/vnotch/calc";

    try {
      var response = await networkUtil.post(url, body: {
        "nilai_pengukuran": calc1,
        "nilai_konstanta": calc2,
        "vnotch_type": calc3,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> calcOW(calc1, calc2) async {
    var url = "${baseUrl}nilai/ow/calc";

    try {
      var response = await networkUtil.post(url, body: {
        "nilai_pengukuran": calc1,
        "nilai_konstanta": calc2,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inPiezo(
      DataPiezoDevices device, date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/piezo/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_pengukuran": field[0],
        "nilai_konstanta": field[1],
        "nilai_pengolahan": field[2],
        "foto": image,
        "keterangan": desc,
      });
      print(response.toString());

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inLeakage(
      DataLeakageDevices device, date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/leakage/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_volume": field[0],
        "nilai_detik": field[1],
        "nilai_pengolahan": field[2],
        "foto": image,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inTDJTower(
      DataTdjtDevices device, date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/tdj-tower/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_pengukuran_a": field[0],
        "nilai_konstanta_a": field[1],
        "nilai_pengolahan_a": field[2],
        "nilai_pengukuran_b": field[3],
        "nilai_konstanta_b": field[4],
        "nilai_pengolahan_b": field[5],
        "nilai_pengukuran_c": field[6],
        "nilai_konstanta_c": field[7],
        "nilai_pengolahan_c": field[8],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inTDJGallery(
      DataTdjgDevices device, date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/tdj-access/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_pengukuran_a": field[0],
        "nilai_konstanta_a": field[1],
        "nilai_pengolahan_a": field[2],
        "nilai_pengukuran_b": field[3],
        "nilai_konstanta_b": field[4],
        "nilai_pengolahan_b": field[5],
        "nilai_pengukuran_c": field[6],
        "nilai_konstanta_c": field[7],
        "nilai_pengolahan_c": field[8],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inVNotch(
      DataVNotchDevices device, date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/vnotch/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_pengukuran": field[0],
        "nilai_konstanta": field[1],
        "nilai_pengolahan": field[2],
        "vnotch_type": field[3],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inCurahHujan(DataCurahHujanDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/curah-hujan/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_st1": field[0],
        "nilai_st2": field[1],
        "nilai_st3": field[2],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inTailrace(
      DataTailraceDevices device, date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/tma-tailrace/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_pengukuran": field[0],
        "vnotch_type": field[3],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inTMAWaduk(
      DataTmawDevices device, date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/tma-waduk/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_pengukuran": field[0],
        "vnotch_type": field[3],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inOW(
      DataOwDevices device, date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/ow/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "nilai_pengukuran": field[0],
        "nilai_konstanta": field[1],
        "nilai_pengolahan": field[2],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> inRingMagnet(DataRingMagnetDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/piezo/save";

    try {
      var response = await networkUtil.post(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "tanggal": date,
        "foto": null,
        "keterangan": desc,
        "ring_id": field[0],
        "nilai_pengukuran": field[1],
        "nilai_konstanta": field[2],
        "nilai_elic": field[3],
        "id_elic": field[4],
        "nilai_elevasi": field[5],
        "nilai_kedalaman_vertikal": field[6],
        "nilai_delta": field[7],
        "nilai_delta_kumulatif": field[8],
      });
      print(response.toString());

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updatePiezo(String itemId, DataPiezoDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/piezo/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_pengukuran": field[0],
        "nilai_konstanta": field[1],
        "nilai_pengolahan": field[2],
        "foto": image,
        "keterangan": desc,
      });
      print(response.toString());

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateLeakage(String itemId, DataLeakageDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/leakage/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_volume": field[0],
        "nilai_detik": field[1],
        "nilai_pengolahan": field[2],
        "foto": image,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateTDJTower(String itemId, DataTdjtDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/tdj-tower/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_pengukuran_a": field[0],
        "nilai_konstanta_a": field[1],
        "nilai_pengolahan_a": field[2],
        "nilai_pengukuran_b": field[3],
        "nilai_konstanta_b": field[4],
        "nilai_pengolahan_b": field[5],
        "nilai_pengukuran_c": field[6],
        "nilai_konstanta_c": field[7],
        "nilai_pengolahan_c": field[8],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateTDJGallery(String itemId, DataTdjgDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/tdj-access/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_pengukuran_a": field[0],
        "nilai_konstanta_a": field[1],
        "nilai_pengolahan_a": field[2],
        "nilai_pengukuran_b": field[3],
        "nilai_konstanta_b": field[4],
        "nilai_pengolahan_b": field[5],
        "nilai_pengukuran_c": field[6],
        "nilai_konstanta_c": field[7],
        "nilai_pengolahan_c": field[8],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateVNotch(String itemId, DataVNotchDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/vnotch/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_pengukuran": field[0],
        "nilai_konstanta": field[1],
        "nilai_pengolahan": field[2],
        "vnotch_type": field[3],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateCurahHujan(String itemId, DataCurahHujanDevices device,
      date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/curah-hujan/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_st1": field[0],
        "nilai_st2": field[1],
        "nilai_st3": field[2],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateTailrace(String itemId, DataTailraceDevices device,
      date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/tma-tailrace/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_pengukuran": field[0],
        "vnotch_type": field[3],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateTMAWaduk(String itemId, DataTmawDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/tma-waduk/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_pengukuran": field[0],
        "vnotch_type": field[3],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateOW(String itemId, DataOwDevices device, date,
      List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/ow/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "nilai_pengukuran": field[0],
        "nilai_konstanta": field[1],
        "nilai_pengolahan": field[2],
        "foto": null,
        "keterangan": desc,
      });

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }

  Future<dynamic> updateRingMagnet(String itemId, DataRingMagnetDevices device,
      date, List<String> field, image, desc) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseUrl}nilai/piezo/save/$itemId";

    try {
      var response = await networkUtil.put(url, body: {
        "perangkat_id": device.id,
        "user_id": uData.id,
        "nama_perangkat": device.name,
        "foto": null,
        "keterangan": desc,
        "ring_id": field[0],
        "nilai_pengukuran": field[1],
        "nilai_konstanta": field[2],
        "nilai_elic": field[3],
        "id_elic": field[4],
        "nilai_elevasi": field[5],
        "nilai_kedalaman_vertikal": field[6],
        "nilai_delta": field[7],
        "nilai_delta_kumulatif": field[8],
      });
      print(response.toString());

      return response["data"];
    } catch (e) {
      return throw e;
    }
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:bendungan_hebat/model/UserData.dart';
import 'package:bendungan_hebat/utils/configRestApi.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'baseServices.dart';

class AuthServices extends BaseServices {
  // Network Util
  final ConfigRestApi networkUtil = new ConfigRestApi();

  Future<dynamic> signIn({String? email, String? password}) async {
    var url = "${baseUrl}user/login";
    var prefs = await SharedPreferences.getInstance();

    print(url.toString());

    try {
      var response = await networkUtil.post(
        url,
        body: {"email": email, "password": password},
      );
      print(response.toString());

      final UserData data = UserData.fromJson(response["data"]);
      prefs.setString("user_data", json.encode(data));
      prefs.setBool("islogin", true);

      return data;
    } catch (e) {
      return throw e;
    }
  }

  // Future<dynamic> signUp({String email, password, name, File picture}) async {
  //   var url = "${baseUrlAuth}signup";
  //   var prefs = await SharedPreferences.getInstance();

  //   try {
  //     var response = await networkUtil.post(
  //       url,
  //       body: {
  //         "email": email,
  //         "password": password,
  //         "confirmPassword": password,
  //         "profile": {
  //           "name": name,
  //         }
  //       },
  //     );

  //     prefs.setString("refreshToken", response["refreshToken"]);
  //     prefs.setString("token", response["token"]);

  //     var data = await getUserData();

  //     prefs.setBool("islogin", true);

  //     return data;
  //   } catch (e) {
  //     return throw e;
  //   }
  // }

  // Future<dynamic> signOut() async {
  //   var prefs = await SharedPreferences.getInstance();

  //   try {
  //     prefs.remove("token");
  //     prefs.remove("refreshToken");
  //     prefs.remove("user_data");
  //     prefs.remove("islogin");

  //     return;
  //   } catch (e) {
  //     return throw e;
  //   }
  // }

  Future<UserData> getUserData() async {
    var url = "${baseUrl}user/profile";
    var prefs = await SharedPreferences.getInstance();
    UserData uData = await getCachedUserData();

    try {
      var response = await networkUtil.post(url, body: {
        "email": uData.email,
      });

      final UserData data = UserData.fromJson(response["data"]);
      prefs.setString("user_data", json.encode(data));

      return data;
    } catch (e) {
      return throw e;
    }
  }

  Future<UserData> getCachedUserData() async {
    var prefs = await SharedPreferences.getInstance();

    try {
      var response = prefs.getString("user_data");

      final UserData data = UserData.fromJson(json.decode(response!));

      return data;
    } catch (e) {
      return throw e;
    }
  }
}

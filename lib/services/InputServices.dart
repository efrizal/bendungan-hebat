import 'dart:convert';
import 'dart:io';

import 'package:bendungan_hebat/model/UserData.dart';
import 'package:bendungan_hebat/utils/configRestApi.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' as http;

import 'authServices.dart';
import 'baseServices.dart';

class InputServices extends BaseServices {
  // Network Util
  final ConfigRestApi networkUtil = new ConfigRestApi();
  var dio = Dio();

  Future<dynamic> uploadImgData(
    String path,
    File picture,
    List<String>? field,
  ) async {
    UserData uData = await AuthServices().getCachedUserData();
    var url = "${baseFileUrl}api/store-file";
    print(url);

    try {
      var formData = FormData.fromMap(
        {
          "foto": MultipartFile.fromFile(
            picture.path,
            filename: picture.path.substring(picture.path.lastIndexOf("/") + 1),
          ),
          "foto_old": "report_nilai/$path",
          "id_nilai": (field == null) ? "" : field[0],
          "device_type_id": (field == null) ? "" : field[1],
          "user_id": uData.id,
          "user_nama": uData.name,
        },
      );

      var response = await networkUtil.post(
        url,
        body: formData,
      );

      print(response.toString());

      return response;
    } catch (e) {
      return throw e;
    }
  }

  upImgHttp(
    String path,
    File file,
    List<String>? field,
  ) async {
    UserData uData = await AuthServices().getCachedUserData();
    ;
    var request = http.MultipartRequest(
      'POST',
      Uri.parse("${baseFileUrl}api/store-file"),
    );
    Map<String, String> headers = {
      "Content-type": "multipart/form-data",
    };
    request.headers.addAll(headers);
    request.files.add(
      http.MultipartFile(
        'foto',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        filename: file.path.substring(file.path.lastIndexOf("/") + 1),
        contentType: MediaType('image', 'jpg'),
      ),
    );
    request.fields.addAll({
      "foto_old": "report_nilai/$path",
      "id_nilai": (field == null) ? "" : field[0],
      "device_type_id": (field == null) ? "" : field[1],
      "user_id": uData.id!,
      "user_nama": uData.name!,
    });
    var res = await request.send();
    var responseString = await res.stream.bytesToString();
    var responseData = json.decode(responseString);

    return responseData["file"]["foto"];
  }
}

import 'package:bendungan_hebat/utils/config.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BaseServices {
  //TODO: CHANGE PRODUCTION/DEV ENVIRONTMENT
  static bool isDev = false;

  String baseUrl = Config().getBaseUrl();
  String baseFileUrl = Config().getBaseFileUrl();
  String userGroup = "";
  String paymentKey = "";
}

import 'package:get/get.dart';

class RefreshDataController extends GetxController {
  RxBool updating = false.obs;

  void refreshData(isRefresh) {
    updating.value = isRefresh;
    update();
  }
}

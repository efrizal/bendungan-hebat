import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';

class ConfigRestApi {
  static ConfigRestApi _instance = new ConfigRestApi.internal();
  ConfigRestApi.internal();
  factory ConfigRestApi() => _instance;

  final Dio dio = new Dio(
    BaseOptions(connectTimeout: 15000, receiveTimeout: 15000),
  );

  // HTTP GET Method
  Future<dynamic> get(String url, {Options? options}) async {
    try {
      var response = await dio.get(url, options: options);
      final res = response.data;

      final int? statusCode = response.statusCode;
      if (statusCode! < 200 || statusCode > 400 || statusCode == null) {
        throw new Exception("Error while fetching data");
      }
      return res;
    } on DioError catch (e) {
      throw new Exception(
          "Error while fetching data : [${e.response!.statusCode}] ${e.response.toString()}");
    }
  }

  // HTTP POST Method
  Future<dynamic> post(String url, {body, Options? cOptions}) async {
    try {
      var response = await dio.post(url, data: body, options: cOptions);
      final res = response.data;

      final int? statusCode =
          (response.statusCode == null) ? 200 : response.statusCode;
      if (statusCode! < 200 || statusCode > 400) {
        throw new Exception("Error while fetching data");
      }

      return res;
    } on DioError catch (e) {
      // Clipboard.setData(ClipboardData(
      //     text:
      //         "Error while fetching data : [${e.response!.statusCode}] ${e.response.toString()}"));
      throw new Exception(
          "Error while fetching data : [${e.response!.statusCode}] ${e.response.toString()}");
    }
  }

  // HTTP PUT Method
  Future<dynamic> put(String url, {body, Options? options}) async {
    try {
      var response = await dio.put(url, data: body, options: options);
      final res = response.data;
      final int? statusCode = response.statusCode;
      if (statusCode! < 200 || statusCode > 400) {
        throw new Exception("Error while fetching data");
      }
      return res;
    } on DioError catch (e) {
      throw new Exception(
          "Error while fetching data : [${e.response!.statusCode.toString()}] ${e.response.toString()}");
    }
  }

  // HTTP PATCH Method
  Future<dynamic> patch(String url, {body, Options? options}) async {
    try {
      var response = await dio.patch(url, data: body, options: options);
      final res = response.data;
      final int? statusCode = response.statusCode;
      if (statusCode! < 200 || statusCode > 400) {
        throw new Exception("Error while fetching data");
      }
      return res;
    } on DioError catch (e) {
      throw new Exception(
          "Error while fetching data : [${e.response!.statusCode}] ${e.response.toString()}");
    }
  }
}

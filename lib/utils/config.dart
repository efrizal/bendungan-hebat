import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:hive/hive.dart';

import 'configRestApi.dart';

class Config {
  final ConfigRestApi networkUtil = new ConfigRestApi();

  // Constructor (Singleton)
  static final Config _instance = new Config._internal();
  Config._internal();
  factory Config() => _instance;

  Future<Config> setupRemoteConfig() async {
    try {
      await getConfigData();
    } catch (exception) {
      print('Unable to fetch config. Cached or default values will be used');
      print(exception.toString());
    }

    return this;
  }

  getConfigData() async {
    var url =
        "https://drive.google.com/uc?export=download&id=1sbSoeAdP0cU1Etsb0tMKg41fKrHaCB4V";

    try {
      var response = await networkUtil.get(
        url,
        options: Options(
          headers: {
            "Content-Type": "application/json",
          },
        ),
      );
      var resData = json.decode(response.toString());
      print(resData["base_url"].toString());

      var box = Hive.box('remoteConfig');
      box.put('base_url', resData["base_url"]);
      box.put('file_url', resData["file_url"]);

      return resData["base_url"];
    } catch (e) {
      return throw e;
    }
  }

  getBaseUrl() {
    var box = Hive.box('remoteConfig');
    var data = box.get('base_url');

    return data;
  }

  getBaseFileUrl() {
    var box = Hive.box('remoteConfig');
    var data = box.get('file_url');

    return data;
  }
}

import 'package:flutter/material.dart';

class ConfigTheme {
  static const MaterialColor firstColor = MaterialColor(
    _firstColorPrimaryValue,
    <int, Color>{
      50: Color(0xffe3e4e6),
      100: Color(0xffb9bcc0),
      200: Color(0xff8b9096),
      300: Color(0xff5d646c),
      400: Color(0xff3a424d),
      500: Color(0xff17212d),
      600: Color(0xff141d28),
      700: Color(0xff111822),
      800: Color(0xff0d141c),
      900: Color(0xff070b11),
    },
  );
  static const int _firstColorPrimaryValue = 0xff17212d;

  static const MaterialColor secondColor = MaterialColor(
    _secondColorPrimaryValue,
    <int, Color>{
      500: Color(0xff4396ec),
    },
  );
  static const int _secondColorPrimaryValue = 0xff4396ec;

  static const MaterialColor thirdColor = MaterialColor(
    _thirdColorPrimaryValue,
    <int, Color>{
      500: Color(0xff56bbe9),
    },
  );
  static const int _thirdColorPrimaryValue = 0xff56bbe9;

  // Theme Setting
  static final primaryTheme = ThemeData(
    primarySwatch: firstColor,
    primaryColor: firstColor,
    primaryColorLight: Color(0xff232429),
    accentColor: secondColor,
    scaffoldBackgroundColor: Colors.grey[50],
    canvasColor: Colors.white,
    fontFamily: 'Raleway',
  );
}

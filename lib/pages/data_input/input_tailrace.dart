import 'dart:io';

import 'package:bendungan_hebat/controller/getx_controller.dart';
import 'package:bendungan_hebat/model/TailraceData.dart';
import 'package:bendungan_hebat/model/devices/CurahHujanDeviceData.dart';
import 'package:bendungan_hebat/model/devices/TDJTDeviceData.dart';
import 'package:bendungan_hebat/model/devices/TMATDeviceData.dart';
import 'package:bendungan_hebat/model/devices/TMAWDeviceData.dart';
import 'package:bendungan_hebat/services/damDataServices.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:image_picker/image_picker.dart';

class InputTailrace extends StatefulWidget {
  InputTailrace({Key? key, this.isEdit = false, this.editData})
      : super(key: key);
  DataTailrace? editData;
  bool? isEdit;

  @override
  _InputTailraceState createState() => _InputTailraceState();
}

class _InputTailraceState extends State<InputTailrace> {
  File? _image;
  final _formKey = GlobalKey<FormState>();
  final picker = ImagePicker();
  late TextEditingController _controller1;
  late TextEditingController _controller2;
  late TextEditingController _controller3;
  late TextEditingController _controller4;
  late TextEditingController _controller5;
  List<String> inputField = ["", "", "", "", "", "", "", "", ""];
  String keterangan = "";

  late DateTime selDate = DateTime.now();
  bool isCalculated = false;

  Future getImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  submitData() async {
    // var datas = Hive.box('piezoData');
    // Directory appDocDir = await getApplicationDocumentsDirectory();
    // String path = appDocDir.path;
    // final File? newImage = (_image == null)
    //     ? null
    //     : await _image!.copy('$path/${_image!.path.split('/').last}');

    _formKey.currentState!.save();
    if (selDevice == null ||
        inputField[0] == "" ||
        selDate.toIso8601String().isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Pilih Instrument / Masukkan data terlebih dahulu!!"),
          duration: Duration(seconds: 1)));
    } else {
      if (widget.isEdit!) {
        DamDataServices()
            .updateTailrace(widget.editData!.id.toString(), selDevice!,
                selDate.toIso8601String(), inputField, null, keterangan)
            .then((value) {
          Get.back();
          var rControl = Get.put(RefreshDataController());
          rControl.refreshData(true);

          showDialog(
            barrierDismissible: true,
            context: context,
            builder: (context) {
              return SimpleDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                      top: 20.0,
                      bottom: 0.0,
                    ),
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.check_circle_outline_outlined,
                          size: 100.0,
                          color: ConfigTheme.secondColor,
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "Berhasil Mengubah Data!",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 15.0),
                        ElevatedButton(
                          onPressed: () {
                            Get.back();
                          },
                          style: ElevatedButton.styleFrom(
                            primary: ConfigTheme.secondColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                          child: Text("Tutup"),
                        )
                      ],
                    ),
                  )
                ],
              );
            },
          );
        });
      } else {
        DamDataServices()
            .inTailrace(selDevice!, selDate.toIso8601String(), inputField, null,
                keterangan)
            .then((value) {
          Get.back();

          showDialog(
            barrierDismissible: true,
            context: context,
            builder: (context) {
              return SimpleDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                      top: 20.0,
                      bottom: 0.0,
                    ),
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.check_circle_outline_outlined,
                          size: 100.0,
                          color: ConfigTheme.secondColor,
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "Berhasil Menambahkan Data!",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 15.0),
                        ElevatedButton(
                          onPressed: () {
                            Get.back();
                          },
                          style: ElevatedButton.styleFrom(
                            primary: ConfigTheme.secondColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                          child: Text("Tutup"),
                        )
                      ],
                    ),
                  )
                ],
              );
            },
          );
        });
      }
    }
  }

  List<DataTailraceDevices> deviceDatas = [];
  DataTailraceDevices? selDevice;
  bool isDevLoading = true;
  getDevices() {
    DamDataServices().getTMATDevices().then((value) {
      setState(() {
        isDevLoading = false;
        deviceDatas = value;
        deviceDatas.insert(0, DataTailraceDevices(name: "Pilih Instrument"));
      });
    });
  }

  @override
  void initState() {
    _controller1 = TextEditingController(text: DateTime.now().toString());
    _controller2 = TextEditingController(
        text: (widget.isEdit!)
            ? widget.editData!.nilaiPengukuran.toString()
            : " ");
    _controller3 = TextEditingController(text: " ");
    _controller4 = TextEditingController(text: " ");
    getDevices();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          (widget.isEdit!)
              ? "Ubah Data TMA Tailrace"
              : "Tambah Data TMA Tailrace",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: (isDevLoading)
          ? Container(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nama Instrument", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            SizedBox(width: 10.0),
                            Expanded(
                              child: DropdownButtonFormField(
                                items: deviceDatas.map((f) {
                                  return DropdownMenuItem(
                                    value: f.id.toString(),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        // Icon(Icons.star),
                                        Flexible(
                                          child: Text(
                                            f.name!,
                                            textAlign: TextAlign.end,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                                selectedItemBuilder: (BuildContext context) {
                                  return deviceDatas.map<Widget>((var item) {
                                    return Container(
                                        alignment: Alignment.centerRight,
                                        width: 180,
                                        child: Text(
                                          item.name!,
                                          textAlign: TextAlign.end,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ));
                                  }).toList();
                                },
                                value: deviceDatas[0].id.toString(),
                                onChanged: (newValue) {
                                  print(newValue);
                                  setState(() {
                                    int i = deviceDatas.indexWhere((element) =>
                                        element.id.toString() == newValue);
                                    print(i);
                                    selDevice = deviceDatas[i];
                                    isCalculated = false;
                                  });
                                },
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    filled: true,
                                    fillColor: Colors.transparent,
                                    border: InputBorder.none
                                    // hintText: Localization.of(context).category,
                                    ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Tanggal", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: DateTimePicker(
                                type: DateTimePickerType.date,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                dateMask: 'd MMM yyyy',
                                controller: _controller1,
                                //initialValue: _initialValue,
                                firstDate: DateTime(2000),
                                lastDate: DateTime(2100),
                                textAlign: TextAlign.end,
                                //icon: Icon(Icons.event),
                                // dateLabelText: 'Date Time',
                                use24HourFormat: false,
                                // locale: Locale('en', 'US'),
                                onChanged: (val) => setState(
                                  () => selDate = DateTime.parse(val),
                                ),
                                validator: (val) {
                                  setState(
                                      () => selDate = DateTime.parse(val!));
                                  return null;
                                },
                                onSaved: (val) => setState(
                                  () => () => selDate = DateTime.parse(val!),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nilai TMA Tailrace", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controller2,
                                scrollPadding: EdgeInsets.all(0.0),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'^\d+\.?\,?\d{0,10}')),
                                ],
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                textAlign: TextAlign.right,
                                onChanged: (x) {
                                  setState(() {
                                    inputField[0] = x;
                                  });
                                },
                                onSaved: (input) {
                                  inputField[0] = input!;
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      GestureDetector(
                          onTap: getImage,
                          child: Stack(
                            children: [
                              (_image == null)
                                  ? Container()
                                  : Stack(
                                      children: [
                                        Container(
                                          height: 140.0,
                                          width: Get.width,
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            child: Image.file(
                                              _image!,
                                              height: 140.0,
                                              width: Get.width,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 140.0,
                                          width: Get.width,
                                          decoration: BoxDecoration(
                                            color: Colors.white60,
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                        )
                                      ],
                                    ),
                              Container(
                                height: 140.0,
                                width: Get.width,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.grey,
                                    width: (_image == null) ? 1 : 2,
                                  ),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      (_image != null)
                                          ? Icons.no_photography_outlined
                                          : Icons.add_a_photo_outlined,
                                      size: 35.0,
                                      color: (_image == null)
                                          ? Colors.grey[600]
                                          : Colors.black,
                                    ),
                                    SizedBox(height: 7.0),
                                    Text(
                                      (_image != null)
                                          ? "Ubah Gambar"
                                          : "Tambah Gambar", //TODO: LOCALIZE
                                      style: TextStyle(
                                        color: (_image == null)
                                            ? Colors.grey[600]
                                            : Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                      SizedBox(height: 20.0),
                      Container(
                        height: 140.0,
                        width: Get.width,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(15.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Keterangan", //TODO: LOCALIZE
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                            Expanded(
                              child: TextFormField(
                                scrollPadding: EdgeInsets.all(0.0),
                                scrollPhysics: BouncingScrollPhysics(),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 0.0, vertical: 5.0),
                                  border: InputBorder.none,
                                ),
                                inputFormatters: [],
                                minLines: 1,
                                maxLines: 5,
                                validator: (String? value) {},
                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.left,
                                onChanged: (input) {
                                  setState(() {
                                    keterangan = input;
                                  });
                                },
                                onSaved: (input) {
                                  keterangan = input!;
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 40.0),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: ConfigTheme.secondColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                        ),
                        child: Container(
                          width: Get.width,
                          height: 50.0,
                          alignment: Alignment.center,
                          child: Text(
                            "Kirim Data", //TODO:LOCALIZE
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        onPressed: () {
                          submitData();
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}

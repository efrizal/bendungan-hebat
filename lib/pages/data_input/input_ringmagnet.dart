import 'dart:io';

import 'package:bendungan_hebat/model/ElicData.dart';
import 'package:bendungan_hebat/model/RingMagnetData.dart';
import 'package:bendungan_hebat/model/devices/PiezoDeviceData.dart';
import 'package:bendungan_hebat/model/devices/RingMagnetDeviceData.dart';
import 'package:bendungan_hebat/model/devices/RingTypeDevices.dart';
import 'package:bendungan_hebat/services/damDataServices.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class InputRingMagnet extends StatefulWidget {
  InputRingMagnet({Key? key, this.editData, this.isEdit}) : super(key: key);
  DataRingMagnet? editData;
  bool? isEdit;

  @override
  _InputRingMagnetState createState() => _InputRingMagnetState();
}

class _InputRingMagnetState extends State<InputRingMagnet> {
  File? _image;
  final _formKey = GlobalKey<FormState>();
  final picker = ImagePicker();
  late TextEditingController _controller1;
  late TextEditingController _controller2;
  late TextEditingController _controller3;
  late TextEditingController _controller4;
  late TextEditingController _controller5;
  late TextEditingController _controller6;
  List<String> inputField = ["", "", "", "", "", "", "", "", ""];
  String keterangan = "";

  late DateTime selDate = DateTime.now();
  bool isCalculated = false;

  Future getImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  submitData() async {
    // var datas = Hive.box('piezoData');
    // Directory appDocDir = await getApplicationDocumentsDirectory();
    // String path = appDocDir.path;
    // final File? newImage = (_image == null)
    //     ? null
    //     : await _image!.copy('$path/${_image!.path.split('/').last}');

    _formKey.currentState!.save();

    if (selDevice == null ||
        inputField[0] == "" ||
        inputField[1] == "" ||
        inputField[2] == "" ||
        selDate.toIso8601String().isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Pilih Instrument / Masukkan data terlebih dahulu!!"),
          duration: Duration(seconds: 1)));
    } else {
      if (widget.isEdit!) {
        DamDataServices()
            .updateRingMagnet(widget.editData!.id.toString(), selDevice!,
                selDate.toIso8601String(), inputField, null, keterangan)
            .then((value) {
          Get.back();

          showDialog(
            barrierDismissible: true,
            context: context,
            builder: (context) {
              return SimpleDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                      top: 20.0,
                      bottom: 0.0,
                    ),
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.check_circle_outline_outlined,
                          size: 100.0,
                          color: ConfigTheme.secondColor,
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "Berhasil Menambahkan Data!",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 15.0),
                        ElevatedButton(
                          onPressed: () {
                            Get.back();
                          },
                          style: ElevatedButton.styleFrom(
                            primary: ConfigTheme.secondColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                          child: Text("Tutup"),
                        )
                      ],
                    ),
                  )
                ],
              );
            },
          );
        });
      } else {
        DamDataServices()
            .inRingMagnet(selDevice!, selDate.toIso8601String(), inputField,
                null, keterangan)
            .then((value) {
          Get.back();

          showDialog(
            barrierDismissible: true,
            context: context,
            builder: (context) {
              return SimpleDialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                      top: 20.0,
                      bottom: 0.0,
                    ),
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.check_circle_outline_outlined,
                          size: 100.0,
                          color: ConfigTheme.secondColor,
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "Berhasil Menambahkan Data!",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 15.0),
                        ElevatedButton(
                          onPressed: () {
                            Get.back();
                          },
                          style: ElevatedButton.styleFrom(
                            primary: ConfigTheme.secondColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                          ),
                          child: Text("Tutup"),
                        )
                      ],
                    ),
                  )
                ],
              );
            },
          );
        });
      }
    }
  }

  List<DataRingMagnetDevices> deviceDatas = [];
  DataRingMagnetDevices? selDevice = new DataRingMagnetDevices();
  List<RingTypeDevices> ringTypeDatas = [];
  RingTypeDevices? selRingType = new RingTypeDevices();
  bool isDevLoading = true;
  getDevices() {
    DamDataServices().getRingMDevices().then((value) {
      setState(() {
        deviceDatas = value;
        deviceDatas.insert(0, DataRingMagnetDevices(name: "Pilih Instrument"));
      });
      DamDataServices().getRingTypeDevices().then((value) {
        setState(() {
          isDevLoading = false;
          ringTypeDatas = value;
          ringTypeDatas.insert(0, RingTypeDevices(ringNama: "Pilih Tipe Ring"));
        });
      });
    });
  }

  List<ElicData> elicDatas = [];
  ElicData? selElic;
  getElic() {
    DamDataServices()
        .getElic(selDate.month.toString(), selDate.year.toString(),
            selDevice!.id.toString())
        .then((value) {
      setState(() {
        elicDatas = value;
        elicDatas.insert(
            0, ElicData(devicesNama: "Pilih Elic", tanggal: "Pilih Elic"));
      });
    });
  }

  @override
  void initState() {
    _controller1 = TextEditingController(text: DateTime.now().toString());
    _controller2 = TextEditingController(text: "0");
    _controller3 = TextEditingController(
        text:
            (widget.isEdit!) ? widget.editData!.nilaiElevasi.toString() : "0");
    _controller4 = TextEditingController(text: " ");
    _controller5 = TextEditingController(
        text: (widget.isEdit!) ? widget.editData!.nilaiDelta.toString() : "0");
    _controller6 = TextEditingController(
        text: (widget.isEdit!)
            ? widget.editData!.nilaiDeltaKumulatif.toString()
            : "0");
    elicDatas.insert(
        0,
        ElicData(
          devicesNama: "Pilih Elic",
          tanggal: "Pilih Elic",
        ));
    getDevices();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          (widget.isEdit!)
              ? "Edit Data Ring Magnet"
              : "Tambah Data Ring Magnet",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: (isDevLoading)
          ? Container(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nama Instrument", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            SizedBox(width: 10.0),
                            Expanded(
                              child: DropdownButtonFormField(
                                items: deviceDatas.map((f) {
                                  return DropdownMenuItem(
                                    value: f.id.toString(),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        // Icon(Icons.star),
                                        Flexible(
                                          child: Text(
                                            f.name!,
                                            textAlign: TextAlign.end,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                                selectedItemBuilder: (BuildContext context) {
                                  return deviceDatas.map<Widget>((var item) {
                                    return Container(
                                        alignment: Alignment.centerRight,
                                        width: 180,
                                        child: Text(
                                          item.name!,
                                          textAlign: TextAlign.end,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ));
                                  }).toList();
                                },
                                value: deviceDatas[0].id.toString(),
                                onChanged: (newValue) {
                                  print(newValue);
                                  setState(() {
                                    int i = deviceDatas.indexWhere((element) =>
                                        element.id.toString() == newValue);
                                    print(i);
                                    selDevice = deviceDatas[i];
                                    isCalculated = false;

                                    _controller2 = TextEditingController(
                                        text: selDevice!.nilaiKonstanta
                                            .toString());
                                    _controller4 =
                                        TextEditingController(text: " ");
                                    getElic();
                                  });
                                },
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    filled: true,
                                    fillColor: Colors.transparent,
                                    border: InputBorder.none
                                    // hintText: Localization.of(context).category,
                                    ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Tanggal", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: DateTimePicker(
                                type: DateTimePickerType.date,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                dateMask: 'd MMM yyyy',
                                controller: _controller1,
                                //initialValue: _initialValue,
                                firstDate: DateTime(2000),
                                lastDate: DateTime(2100),
                                textAlign: TextAlign.end,
                                //icon: Icon(Icons.event),
                                // dateLabelText: 'Date Time',
                                use24HourFormat: false,
                                // locale: Locale('en', 'US'),
                                onChanged: (val) {
                                  setState(() => selDate = DateTime.parse(val));
                                  getElic();
                                },
                                validator: (val) {
                                  setState(
                                      () => selDate = DateTime.parse(val!));
                                  return null;
                                },
                                onSaved: (val) => setState(
                                  () => () => selDate = DateTime.parse(val!),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Tipe Ring", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            SizedBox(width: 10.0),
                            Expanded(
                              child: DropdownButtonFormField(
                                items: ringTypeDatas.map((f) {
                                  return DropdownMenuItem(
                                    value: f.id.toString(),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        // Icon(Icons.star),
                                        Flexible(
                                          child: Text(
                                            f.ringNama!,
                                            textAlign: TextAlign.end,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                                selectedItemBuilder: (BuildContext context) {
                                  return ringTypeDatas.map<Widget>((var item) {
                                    return Container(
                                        alignment: Alignment.centerRight,
                                        width: 230,
                                        child: Text(
                                          item.ringNama!,
                                          textAlign: TextAlign.end,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ));
                                  }).toList();
                                },
                                value: ringTypeDatas[0].id.toString(),
                                onChanged: (newValue) {
                                  print(newValue);
                                  setState(() {
                                    int i = ringTypeDatas.indexWhere(
                                        (element) =>
                                            element.id.toString() == newValue);
                                    print(i);
                                    selRingType = ringTypeDatas[i];
                                    isCalculated = false;

                                    _controller4 =
                                        TextEditingController(text: " ");
                                    inputField[0] = selRingType!.id.toString();
                                  });
                                },
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                decoration: InputDecoration(
                                    contentPadding:
                                        EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    filled: true,
                                    fillColor: Colors.transparent,
                                    border: InputBorder.none
                                    // hintText: Localization.of(context).category,
                                    ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nilai Elic", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            SizedBox(width: 10.0),
                            Expanded(
                              child: (selDevice!.id == null ||
                                      selRingType!.id == null)
                                  ? Container(
                                      height: 40.0,
                                      alignment: Alignment.centerRight,
                                      child: Text("..."),
                                    )
                                  : DropdownButtonFormField(
                                      items: elicDatas.map((f) {
                                        return DropdownMenuItem(
                                          value: f.id.toString(),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: <Widget>[
                                              // Icon(Icons.star),
                                              Flexible(
                                                child: Text(
                                                  "${f.tanggal!}" +
                                                      ((f.nilaiPengukuran ==
                                                              null)
                                                          ? ""
                                                          : " :: ${f.nilaiPengukuran}"),
                                                  textAlign: TextAlign.end,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      }).toList(),
                                      selectedItemBuilder:
                                          (BuildContext context) {
                                        return elicDatas.map<Widget>((var f) {
                                          return Container(
                                              alignment: Alignment.centerRight,
                                              width: 238,
                                              child: Text(
                                                "${f.tanggal!}" +
                                                    ((f.nilaiPengukuran == null)
                                                        ? ""
                                                        : " :: ${f.nilaiPengukuran}"),
                                                textAlign: TextAlign.end,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                              ));
                                        }).toList();
                                      },
                                      value: elicDatas[0].id.toString(),
                                      onChanged: (newValue) {
                                        print(newValue);
                                        setState(() {
                                          int i = elicDatas.indexWhere(
                                              (element) =>
                                                  element.id.toString() ==
                                                  newValue);
                                          print(i);
                                          selElic = elicDatas[i];
                                          inputField[3] = selElic!
                                              .nilaiPengukuran
                                              .toString();
                                          inputField[4] =
                                              selElic!.id.toString();

                                          DamDataServices()
                                              .calcRing(
                                                  0.0,
                                                  selDevice!.nilaiKonstanta,
                                                  selElic!.nilaiPengukuran,
                                                  selDate.month,
                                                  selDate.year,
                                                  selRingType!.id)
                                              .then((value) {
                                            _controller3 = TextEditingController(
                                                text: value[
                                                    "nilai_kedalaman_elevasi"]);
                                            _controller5 =
                                                TextEditingController(
                                                    text: value["nilai_delta"]);
                                            _controller6 = TextEditingController(
                                                text: value[
                                                    "nilai_delta_kumulatif"]);
                                          });
                                        });
                                      },
                                      validator: (String? value) {
                                        if (value!.isEmpty) {
                                          return 'Harap masukkan data';
                                        } else
                                          return null;
                                      },
                                      decoration: InputDecoration(
                                          contentPadding:
                                              EdgeInsets.fromLTRB(0, 0, 0, 0),
                                          filled: true,
                                          fillColor: Colors.transparent,
                                          border: InputBorder.none
                                          // hintText: Localization.of(context).category,
                                          ),
                                    ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 7.0),
                      Container(
                        margin: EdgeInsets.only(left: 2.0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "* Pilih Instrument, Ring dan Tanggal untuk menampilkan Data Elic",
                          style: TextStyle(
                            color: Colors.red[300],
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: 13.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nilai Pengukuran", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: TextFormField(
                                scrollPadding: EdgeInsets.all(0.0),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'^\d+\.?\,?\d{0,10}')),
                                ],
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                textAlign: TextAlign.right,
                                onChanged: (x) {
                                  setState(() {
                                    inputField[0] = x;
                                    isCalculated = false;
                                    _controller4 =
                                        TextEditingController(text: " ");
                                    inputField[1] = x.toString();
                                  });
                                },
                                onSaved: (input) {
                                  inputField[0] = input!;
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nilai Konstanta", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controller2,
                                scrollPadding: EdgeInsets.all(0.0),
                                enabled: false,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'^\d+\.?\,?\d{0,10}')),
                                ],
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                textAlign: TextAlign.right,
                                onSaved: (input) {
                                  inputField[2] = input!;
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(15.0),
                          color: Colors.grey[200],
                        ),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Level Kedalaman Vertikal (m)", //TODO: LOCALIZE
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controller4,
                                scrollPadding: EdgeInsets.all(0.0),
                                enabled: false,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(
                                      RegExp(r'^\d+\.?\,?\d{0,10}')),
                                ],
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                textAlign: TextAlign.right,
                                onChanged: (input) {
                                  setState(() {
                                    inputField[6] = input;
                                  });
                                },
                                onSaved: (input) {
                                  setState(() {
                                    inputField[6] = input!;
                                  });
                                },
                              ),
                            ),
                            (isCalculated)
                                ? Container()
                                : ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        primary: ConfigTheme.secondColor,
                                        padding: EdgeInsets.symmetric(
                                          vertical: 0.0,
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(100.0))),
                                    onPressed: () {
                                      if (selDevice != null &&
                                          inputField[0].isNotEmpty) {
                                        DamDataServices()
                                            .calcRing(inputField[0],
                                                selDevice!.nilaiKonstanta)
                                            .then((val) {
                                          setState(() {
                                            isCalculated = true;
                                            _controller4 = TextEditingController(
                                                text:
                                                    "${val['nilai_kedalaman_vertikal']}");
                                          });
                                        });
                                      }
                                    },
                                    child: Text(
                                      "Hitung",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nilai Kedalaman Elevasi (m)",
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controller3,
                                scrollPadding: EdgeInsets.all(0.0),
                                enabled: false,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                textAlign: TextAlign.right,
                                onChanged: (input) {
                                  setState(() {
                                    inputField[5] = input;
                                  });
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nilai Δ (+/-)",
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controller5,
                                scrollPadding: EdgeInsets.all(0.0),
                                enabled: false,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                textAlign: TextAlign.right,
                                onChanged: (input) {
                                  setState(() {
                                    inputField[7] = input;
                                  });
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        // height: 40.0,
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(15.0)),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "Nilai Δ Kumulatif",
                              style: TextStyle(color: Colors.grey[600]),
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: _controller6,
                                scrollPadding: EdgeInsets.all(0.0),
                                enabled: false,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 5.0, vertical: 0.0),
                                  border: InputBorder.none,
                                ),
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                textAlign: TextAlign.right,
                                onChanged: (input) {
                                  setState(() {
                                    inputField[8] = input;
                                  });
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      GestureDetector(
                          onTap: getImage,
                          child: Stack(
                            children: [
                              (_image == null)
                                  ? Container()
                                  : Stack(
                                      children: [
                                        Container(
                                          height: 140.0,
                                          width: Get.width,
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                            child: Image.file(
                                              _image!,
                                              height: 140.0,
                                              width: Get.width,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: 140.0,
                                          width: Get.width,
                                          decoration: BoxDecoration(
                                            color: Colors.white60,
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                        )
                                      ],
                                    ),
                              Container(
                                height: 140.0,
                                width: Get.width,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.grey,
                                    width: (_image == null) ? 1 : 2,
                                  ),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      (_image != null)
                                          ? Icons.no_photography_outlined
                                          : Icons.add_a_photo_outlined,
                                      size: 35.0,
                                      color: (_image == null)
                                          ? Colors.grey[600]
                                          : Colors.black,
                                    ),
                                    SizedBox(height: 7.0),
                                    Text(
                                      (_image != null)
                                          ? "Ubah Gambar"
                                          : "Tambah Gambar", //TODO: LOCALIZE
                                      style: TextStyle(
                                        color: (_image == null)
                                            ? Colors.grey[600]
                                            : Colors.black,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )),
                      SizedBox(height: 20.0),
                      Container(
                        height: 140.0,
                        width: Get.width,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(15.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Keterangan", //TODO: LOCALIZE
                              style: TextStyle(
                                color: Colors.grey[600],
                              ),
                            ),
                            Expanded(
                              child: TextFormField(
                                scrollPadding: EdgeInsets.all(0.0),
                                scrollPhysics: BouncingScrollPhysics(),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 0.0, vertical: 5.0),
                                  border: InputBorder.none,
                                ),
                                inputFormatters: [],
                                minLines: 1,
                                maxLines: 5,
                                validator: (String? value) {
                                  if (value!.isEmpty) {
                                    return 'Harap masukkan data';
                                  } else
                                    return null;
                                },
                                keyboardType: TextInputType.text,
                                textAlign: TextAlign.left,
                                onChanged: (input) {
                                  setState(() {
                                    keterangan = input;
                                  });
                                },
                                onSaved: (input) {
                                  keterangan = input!;
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 40.0),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: ConfigTheme.secondColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                        ),
                        child: Container(
                          width: Get.width,
                          height: 50.0,
                          alignment: Alignment.center,
                          child: Text(
                            "Kirim Data", //TODO:LOCALIZE
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        onPressed: () {
                          submitData();
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}

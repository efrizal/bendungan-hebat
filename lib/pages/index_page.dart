import 'package:bendungan_hebat/model/DamCategoryData.dart';
import 'package:bendungan_hebat/model/UserData.dart';
import 'package:bendungan_hebat/pages/profile_page.dart';
import 'package:bendungan_hebat/services/authServices.dart';
import 'package:bendungan_hebat/services/damDataServices.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'homepage/homepage.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  int selArticleBanner = 0;
  UserData uData = new UserData();
  List<DamCategoryData> damDatas = [];
  bool isLoadUser = true, isLoadDamCategory = true;

  getUserData() async {
    try {
      var _uData = await AuthServices().getUserData();
      setState(() {
        uData = _uData;
        isLoadUser = false;
      });
    } catch (e) {
      print(e.toString());
    }
  }

  getDamCategory() async {
    try {
      var _damDatas = await DamDataServices().getDamCategory();
      setState(() {
        damDatas = _damDatas;
        isLoadDamCategory = false;
      });
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  void initState() {
    if (mounted) {
      getUserData();
      getDamCategory();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50]!,
      body: Container(
        width: Get.width,
        height: Get.height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 45.0),
              (isLoadUser)
                  ? Container()
                  : Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          SizedBox(width: 25.0),
                          Image.asset(
                            "assets/images/main-logo.png",
                            width: 50.0,
                            height: 50.0,
                            // fit: BoxFit.cover,
                          ),
                          Expanded(child: Container()),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                uData.name!,
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                uData.email!,
                                style: TextStyle(
                                  fontSize: 13.0,
                                  // fontWeight: FontWeight.bold,
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(width: 15.0),
                          GestureDetector(
                            onTap: () {
                              Get.to(ProfilePage());
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(1000),
                              child: Image.network(
                                uData.avatar!,
                                width: 50.0,
                                height: 50.0,
                                fit: BoxFit.cover,
                                errorBuilder: (ctx, o, s) {
                                  return Image.asset(
                                    "assets/images/no-image.jpeg",
                                    width: 50.0,
                                    height: 50.0,
                                    fit: BoxFit.cover,
                                  );
                                },
                              ),
                            ),
                          ),
                          SizedBox(width: 20.0),
                        ],
                      ),
                    ),
              SizedBox(height: 20.0),
              // Container(
              //   child: Row(
              //     children: [
              //       SizedBox(width: 20.0),
              //       Expanded(
              //         child: Text(
              //           "Unggahan File Terbaru",
              //           style: TextStyle(
              //             fontSize: 16.0,
              //             fontWeight: FontWeight.bold,
              //           ),
              //         ),
              //       ),
              //       SizedBox(width: 5.0),
              //       Container(
              //         child: TextButton(
              //             style: TextButton.styleFrom(
              //                 primary: ConfigTheme.secondColor,
              //                 shape: RoundedRectangleBorder(
              //                   borderRadius: BorderRadius.circular(20.0),
              //                 )),
              //             onPressed: () {},
              //             child: Row(
              //               children: [
              //                 Text(
              //                   "Lihat Semua",
              //                   textAlign: TextAlign.center,
              //                 ),
              //                 Icon(
              //                   Icons.chevron_right_rounded,
              //                   color: ConfigTheme.secondColor,
              //                 ),
              //               ],
              //             )),
              //       ),
              //       SizedBox(width: 5.0),
              //     ],
              //   ),
              // ),
              // SizedBox(height: 5.0),
              // Container(
              //   height: 120.0,
              //   width: Get.width,
              //   child: CarouselSlider.builder(
              //     options: CarouselOptions(
              //       viewportFraction: 0.8,
              //       height: 1020.0,
              //       onPageChanged: (i, reason) {
              //         setState(() {
              //           selArticleBanner = i;
              //         });
              //       },
              //     ),
              //     itemCount: 2,
              //     itemBuilder: (ctx, i, x) {
              //       return GestureDetector(
              //         onTap: () {},
              //         child: Container(
              //           margin: EdgeInsets.symmetric(horizontal: 5.0),
              //           decoration: BoxDecoration(
              //             color: Colors.grey[200],
              //             borderRadius: BorderRadius.circular(15.0),
              //           ),
              //           child: Stack(
              //             children: <Widget>[
              //               // ClipRRect(
              //               //   borderRadius: BorderRadius.circular(15.0),
              //               //   child: CachedNetworkImage(
              //               //     imageUrl: (i % 2 == 0)
              //               //         ? "http://www.jasatirta2.co.id/src/frontend/images/galeri/kk/46fc8b95e98d4a1809ed7a49ff9c592c.png"
              //               //         : "http://www.jasatirta2.co.id/src/frontend/images/galeri/kk/Picture42.png",
              //               //     height: 150.0,
              //               //     width: Get.width,
              //               //     fit: BoxFit.cover,
              //               //   ),
              //               // ),
              //               Container(
              //                 child: Row(
              //                   mainAxisAlignment: MainAxisAlignment.center,
              //                   children: [
              //                     SizedBox(width: 15.0),
              //                     Icon(
              //                       MdiIcons.filePdfOutline,
              //                       size: 50.0,
              //                     ),
              //                     SizedBox(width: 15.0),
              //                     Expanded(
              //                       child: Column(
              //                         mainAxisAlignment:
              //                             MainAxisAlignment.center,
              //                         crossAxisAlignment:
              //                             CrossAxisAlignment.start,
              //                         children: [
              //                           Text(
              //                             "File Informasi Perawatan Bendungan  - ${i + 1}",
              //                             style: TextStyle(
              //                               fontSize: 16.0,
              //                               fontWeight: FontWeight.bold,
              //                             ),
              //                           ),
              //                           SizedBox(height: 5.0),
              //                           Text(
              //                             "Size : 4.670kb",
              //                             style: TextStyle(
              //                               fontSize: 12.0,
              //                               color: Colors.grey[600],
              //                             ),
              //                           ),
              //                           SizedBox(height: 5.0),
              //                           Row(
              //                             children: [
              //                               Icon(
              //                                 Icons.calendar_today_rounded,
              //                                 size: 15.0,
              //                                 color: Colors.grey[600],
              //                               ),
              //                               SizedBox(width: 3.0),
              //                               Text(
              //                                 "Size : 4.670kb",
              //                                 style: TextStyle(
              //                                   fontSize: 12.0,
              //                                   color: Colors.grey[600],
              //                                 ),
              //                               ),
              //                               SizedBox(width: 15.0),
              //                               Icon(
              //                                 Icons.person,
              //                                 size: 15.0,
              //                                 color: Colors.grey[600],
              //                               ),
              //                               SizedBox(width: 3.0),
              //                               Text(
              //                                 "Administrator",
              //                                 style: TextStyle(
              //                                   color: Colors.grey[600],
              //                                   fontSize: 12.0,
              //                                 ),
              //                               ),
              //                             ],
              //                           )
              //                         ],
              //                       ),
              //                     )
              //                   ],
              //                 ),
              //               ),
              //               AnimatedOpacity(
              //                 opacity: (selArticleBanner != i) ? 1.0 : 0.0,
              //                 duration: Duration(milliseconds: 100),
              //                 child: Container(
              //                   decoration: BoxDecoration(
              //                     color: Colors.white60,
              //                     borderRadius: BorderRadius.circular(15.0),
              //                   ),
              //                 ),
              //               )
              //             ],
              //           ),
              //         ),
              //       );
              //     },
              //   ),
              // ),
              // SizedBox(height: 15.0),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 6.0),
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          border: Border.all(
                            color: Colors.grey,
                            width: 1.5,
                          ),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Row(
                          children: [
                            SizedBox(width: 10.0),
                            Expanded(
                              child: Text(
                                "Bagikan File...",
                                style: TextStyle(
                                  fontSize: 12.0,
                                ),
                              ),
                            ),
                            Container(
                              height: 26.0,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: ConfigTheme.secondColor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    )),
                                onPressed: () {},
                                child: Text(
                                  "Unggah",
                                  style: TextStyle(
                                    fontSize: 12.0,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 10.0),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20.0),
              (isLoadDamCategory)
                  ? Container(
                      alignment: Alignment.center,
                      height: Get.height * 0.45,
                      width: Get.width,
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      primary: false,
                      padding: EdgeInsets.all(0.0),
                      shrinkWrap: true,
                      itemCount: damDatas.length,
                      itemBuilder: (ctx, i) {
                        return GestureDetector(
                          onTap: () {
                            Get.to(HomePage(
                              openIndex: i,
                              title: "${damDatas[i].type}",
                            ));
                          },
                          child: Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: 20.0,
                              vertical: 7.5,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20.0),
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 5.0,
                                  color: Colors.black12,
                                  offset: Offset(0.0, 0.0),
                                  spreadRadius: 1.0,
                                )
                              ],
                            ),
                            child: IntrinsicHeight(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Container(
                                    width: 80.0,
                                    constraints: BoxConstraints(
                                      minHeight: 80.0,
                                    ),
                                    decoration: BoxDecoration(
                                      color: ConfigTheme.secondColor,
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20.0),
                                        bottomLeft: Radius.circular(20.0),
                                      ),
                                    ),
                                    child: Icon(
                                      MdiIcons.engine,
                                      size: 30.0,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(width: 20.0),
                                  Expanded(
                                    child: Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 10.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            damDatas[i].type!,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          SizedBox(height: 3.0),
                                          (damDatas[i].description == null)
                                              ? Container()
                                              : Text(
                                                  damDatas[i].description!,
                                                  style: TextStyle(
                                                    fontSize: 12.0,
                                                    // fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10.0),
                                  Icon(
                                    Icons.chevron_right,
                                    color: Colors.grey[400]!,
                                  ),
                                  SizedBox(width: 10.0),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
              SizedBox(height: Get.height * 0.05),
              Text(
                "ICT Dam Control Center © Copyright 2021",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.grey[400]!,
                ),
              ),
              SizedBox(height: Get.height * 0.05),
            ],
          ),
        ),
      ),
    );
  }
}

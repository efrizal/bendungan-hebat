import 'package:bendungan_hebat/pages/sign_in.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    if (mounted) {
      fetchUserData();
    }

    super.initState();
  }

  fetchUserData() async {
    // bool _isAuthenticated = await ConfigAuth().authenticated();
    // setState(() => isAuthenticated = _isAuthenticated);

    // if (isAuthenticated) {
    //   ConfigAuth().getUserData().then((onValue) {
    //     setState(() {
    //       userData = onValue;
    //       isloading = false;
    //     });
    //   });
    // } else {
    //   setState(() => isloading = false);
    // }
  }

  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = GlobalKey<ScaffoldState>();
    // UserData userData = new UserData();
    bool isloading = false;
    bool isAuthenticated = true;

    profilePicView() {
      return Stack(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(1000.0),
            child: Image.network(
              "https://images.pexels.com/photos/1680172/pexels-photo-1680172.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
              width: 120.0,
              height: 120.0,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
            bottom: 0.0,
            right: 10.0,
            child: GestureDetector(
              onTap: () async {
                // var image =
                //     await ImagePicker().getImage(source: ImageSource.gallery);

                // if (image != null) {
                //   ConfigAuth().uploadProfile(File(image.path)).then((onValue) {
                //     setState(() {
                //       print(onValue.profilePicture);
                //       userData.user = onValue;
                //     });
                //   });
                // }
              },
              child: Container(
                decoration: BoxDecoration(
                  color: ConfigTheme.thirdColor,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  MdiIcons.circleEditOutline,
                  color: Colors.white,
                  size: 15.0,
                ),
                padding: EdgeInsets.all(5.0),
              ),
            ),
          )
        ],
      );
    }

    _verifyWarning() => Container(
          width: 270.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.red[200],
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  "Akun belum terverifikasi!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12.0,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white24,
                  border: Border.all(
                    color: Colors.red[200]!,
                    width: 1.0,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 7.0),
                child: InkWell(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(10.0),
                    topRight: Radius.circular(10.0),
                  ),
                  onTap: () async {
                    // await ConfigAuth().sendVerificationEmail();

                    // _scaffoldKey.currentState.showSnackBar(SnackBar(
                    //     content: Text(
                    //   "Harap cek e-mail anda",
                    // )));
                  },
                  child: Text(
                    "Verifikasi",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0,
                    ),
                  ),
                ),
              )
            ],
          ),
        );

    profileDet() => Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              profilePicView(),
              SizedBox(height: 15.0),
              Text(
                "Administrator",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 2.0),
              Text(
                "admin@admin.com",
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 12.0,
                ),
              ),
              SizedBox(height: 10.0),
            ],
          ),
        );

    loginRegisterBtn() => Column(
          children: <Widget>[
            RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              color: Colors.grey[200],
              padding: EdgeInsets.all(0.0),
              onPressed: () {
                // Navigator.push(context, NoTransRoute(target: SignInPage()));
              },
              child: Container(
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 15.0),
                    Icon(
                      MdiIcons.login,
                      color: Colors.grey[600],
                    ),
                    SizedBox(width: 15.0),
                    Expanded(
                      child: Text(
                        "Masuk",
                        style: TextStyle(
                          color: Colors.grey[600],
                          // fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      height: 50.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                        color: ConfigTheme.thirdColor,
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(10.0),
                          topRight: Radius.circular(10.0),
                        ),
                      ),
                      child: Icon(
                        Icons.chevron_right,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 15.0),
            RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              color: Colors.grey[200],
              padding: EdgeInsets.all(0.0),
              onPressed: () {
                // Navigator.push(context, NoTransRoute(target: SignUpPage()));
              },
              child: Container(
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 15.0),
                    Icon(Icons.person_add, color: Colors.grey[600]),
                    SizedBox(width: 15.0),
                    Expanded(
                      child: Text(
                        "Daftar",
                        style: TextStyle(color: Colors.grey[600]
                            // fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                    Container(
                      height: 50.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                        color: ConfigTheme.thirdColor,
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(10.0),
                          topRight: Radius.circular(10.0),
                        ),
                      ),
                      child: Icon(
                        Icons.chevron_right,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 15.0),
          ],
        );

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Akun Anda",
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: (isloading)
          ? Center(
              child: CircularProgressIndicator(),
            )
          : CustomScrollView(
              physics: BouncingScrollPhysics(),
              slivers: <Widget>[
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      (!isAuthenticated)
                          ? Container(height: 50.0)
                          : profileDet(),
                      SizedBox(height: 30.0),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 30.0),
                        child: Column(
                          children: <Widget>[
                            (isAuthenticated)
                                ? Container()
                                : loginRegisterBtn(),
                            RaisedButton(
                              onPressed: () {
                                // Navigator.push(
                                //     context, NoTransRoute(target: AboutApps()));
                              },
                              padding: EdgeInsets.all(0.0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              color: Colors.grey[200],
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(width: 15.0),
                                    Icon(
                                      MdiIcons.informationOutline,
                                      color: Colors.grey[600],
                                    ),
                                    SizedBox(width: 15.0),
                                    Expanded(
                                      child: Text(
                                        "Tentang Aplikasi",
                                        style: TextStyle(
                                          color: Colors.grey[600],
                                          // fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 50.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                        color: ConfigTheme.thirdColor,
                                        borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(10.0),
                                          topRight: Radius.circular(10.0),
                                        ),
                                      ),
                                      child: Icon(
                                        Icons.chevron_right,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 15.0),
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              color: Colors.grey[200],
                              padding: EdgeInsets.all(0),
                              onPressed: () {
                                // Navigator.push(
                                //     context, NoTransRoute(target: HelpPage()));
                              },
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(width: 15.0),
                                    Icon(
                                      MdiIcons.helpCircleOutline,
                                      color: Colors.grey[600],
                                    ),
                                    SizedBox(width: 15.0),
                                    Expanded(
                                      child: Text(
                                        "Bantuan",
                                        style: TextStyle(
                                          color: Colors.grey[600],
                                          // fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 50.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                        color: ConfigTheme.thirdColor,
                                        borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(10.0),
                                          topRight: Radius.circular(10.0),
                                        ),
                                      ),
                                      child: Icon(
                                        Icons.chevron_right,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 15.0),
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              color: Colors.grey[200],
                              padding: EdgeInsets.all(0.0),
                              onPressed: () {
                                // Navigator.push(context,
                                //     NoTransRoute(target: TermsAndCondition()));
                              },
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(width: 15.0),
                                    Icon(
                                      MdiIcons.bookmarkOutline,
                                      color: Colors.grey[600],
                                    ),
                                    SizedBox(width: 15.0),
                                    Expanded(
                                      child: Text(
                                        "Syarat Dan Ketentuan",
                                        style: TextStyle(
                                          color: Colors.grey[600],
                                          // fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 50.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                        color: ConfigTheme.thirdColor,
                                        borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(10.0),
                                          topRight: Radius.circular(10.0),
                                        ),
                                      ),
                                      child: Icon(
                                        Icons.chevron_right,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 15.0),
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              color: Colors.grey[200],
                              padding: EdgeInsets.all(0.0),
                              onPressed: () {
                                // Navigator.push(context,
                                //     NoTransRoute(target: PrivacyPolicy()));
                              },
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(width: 15.0),
                                    Icon(
                                      Icons.person_outline,
                                      color: Colors.grey[600],
                                    ),
                                    SizedBox(width: 15.0),
                                    Expanded(
                                      child: Text(
                                        "Kebijakan Privasi",
                                        style: TextStyle(
                                          color: Colors.grey[600],
                                          // fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      height: 50.0,
                                      width: 40.0,
                                      decoration: BoxDecoration(
                                        color: ConfigTheme.thirdColor,
                                        borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(10.0),
                                          topRight: Radius.circular(10.0),
                                        ),
                                      ),
                                      child: Icon(
                                        Icons.chevron_right,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 15.0),
                            // RaisedButton(
                            //   shape: RoundedRectangleBorder(
                            //       borderRadius: BorderRadius.circular(10.0)),
                            //   color: Colors.grey[200],
                            //   padding: EdgeInsets.all(0.0),
                            //   onPressed: () async {
                            //     // var phoneNumber = Config().getContactNumber();
                            //     // var url = "http://wa.me/$phoneNumber";

                            //     // if (await canLaunch(url)) {
                            //     //   await launch(url);
                            //     // } else {
                            //     //   throw 'Could not launch $url';
                            //     // }
                            //   },
                            //   child: Container(
                            //     child: Row(
                            //       children: <Widget>[
                            //         SizedBox(width: 15.0),
                            //         Icon(
                            //           MdiIcons.phoneMessageOutline,
                            //           color: Colors.grey[600],
                            //         ),
                            //         SizedBox(width: 15.0),
                            //         Expanded(
                            //           child: Text(
                            //             "Hubungi Kami",
                            //             style: TextStyle(
                            //               color: Colors.grey[600],
                            //               // fontWeight: FontWeight.bold,
                            //             ),
                            //           ),
                            //         ),
                            //         Container(
                            //           height: 50.0,
                            //           width: 40.0,
                            //           decoration: BoxDecoration(
                            //             color: ConfigTheme.thirdColor,
                            //             borderRadius: BorderRadius.only(
                            //               bottomRight: Radius.circular(10.0),
                            //               topRight: Radius.circular(10.0),
                            //             ),
                            //           ),
                            //           child: Icon(
                            //             Icons.chevron_right,
                            //             color: Colors.white,
                            //           ),
                            //         ),
                            //       ],
                            //     ),
                            //   ),
                            // ),
                            SizedBox(height: 50.0),
                            (!isAuthenticated)
                                ? Container()
                                : GestureDetector(
                                    onTap: () async {
                                      // var url = "https://www.kaaba.co.id/";

                                      // if (await canLaunch(url)) {
                                      //   await launch(url);
                                      // } else {
                                      //   throw 'Could not launch $url';
                                      // }
                                    },
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          "Bendungan Hebat",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey[400],
                                          ),
                                        ),
                                        Text(
                                          "Versi 0.0.1",
                                          style: TextStyle(
                                            color: Colors.grey[400],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
      bottomNavigationBar: (!isAuthenticated)
          ? Container(
              height: 64.0,
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () async {
                      // var url = "https://www.kaaba.co.id/";

                      // if (await canLaunch(url)) {
                      //   await launch(url);
                      // } else {
                      //   throw 'Could not launch $url';
                      // }
                    },
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Powered by",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey[400],
                          ),
                        ),
                        Text(
                          "KAABA Virtual Experience",
                          style: TextStyle(
                            color: Colors.grey[400],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 30.0),
                  Container(
                    height: 3.0,
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            color: ConfigTheme.thirdColor,
                            height: 5.0,
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.2,
                          color: ConfigTheme.secondColor,
                          height: 5.0,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          : Container(
              height: 81.0,
              child: Column(
                children: <Widget>[
                  Container(
                    color: Colors.grey,
                    width: MediaQuery.of(context).size.width,
                    height: 1.0,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: () async {
                            if (!isloading) {
                              // Navigator.push(
                              //     context,
                              //     NoTransRoute(
                              //         target: EditProfilePage(
                              //       data: userData,
                              //     )));
                            }
                          },
                          child: Container(
                            color: Colors.grey[200],
                            width: MediaQuery.of(context).size.width * 0.5,
                            height: 80.0,
                            alignment: Alignment.center,
                            child: Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: <Widget>[
                                Icon(MdiIcons.accountSettingsOutline,
                                    size: 18.0),
                                SizedBox(width: 10.0),
                                Text("Edit Profil"),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 1.0,
                        color: Colors.grey[400],
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () async {
                            showDialog(
                              context: context,
                              builder: (context) => new AlertDialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0)),
                                content: new Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(
                                      MdiIcons.logout,
                                      color: Theme.of(context).primaryColor,
                                      size: 25.0,
                                    ),
                                    SizedBox(width: 8.0),
                                    Expanded(
                                      child: Text(
                                        "Apa kamu ingin keluar?",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 17.0,
                                            color: Colors.black),
                                      ),
                                    ),
                                  ],
                                ),
                                actions: <Widget>[
                                  new FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pop(false);
                                    },
                                    child: new Text('Tidak',
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontSize: 17.0)),
                                  ),
                                  new FlatButton(
                                    onPressed: () async {
                                      var prefs =
                                          await SharedPreferences.getInstance();
                                      await prefs.clear();

                                      Get.offAll(SignInEmail());
                                    },
                                    child: new Text('Keluar',
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontSize: 17.0)),
                                  ),
                                ],
                              ),
                            );
                          },
                          child: Container(
                            color: Colors.grey[200],
                            width: MediaQuery.of(context).size.width * 0.5,
                            height: 80.0,
                            alignment: Alignment.center,
                            child: Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: <Widget>[
                                Icon(MdiIcons.logout, size: 18.0),
                                SizedBox(width: 10.0),
                                Text("Keluar"),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              )),
    );
  }
}

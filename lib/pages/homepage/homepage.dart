import 'dart:async';

import 'package:bendungan_hebat/controller/getx_controller.dart';
import 'package:bendungan_hebat/model/ICHData.dart';
import 'package:bendungan_hebat/model/LeakageData.dart';
import 'package:bendungan_hebat/model/OWData.dart';
import 'package:bendungan_hebat/model/PiezometerData.dart';
import 'package:bendungan_hebat/model/RingMagnetData.dart';
import 'package:bendungan_hebat/model/TDJTowerData.dart';
import 'package:bendungan_hebat/model/TMAWData.dart';
import 'package:bendungan_hebat/model/TailraceData.dart';
import 'package:bendungan_hebat/model/ThreeDimensionData.dart';
import 'package:bendungan_hebat/model/VNotchData.dart';
import 'package:bendungan_hebat/pages/data_input/input_3dt.dart';
import 'package:bendungan_hebat/pages/data_input/input_curahhujan.dart';
import 'package:bendungan_hebat/pages/data_input/input_leakage.dart';
import 'package:bendungan_hebat/pages/data_input/input_ow.dart';
import 'package:bendungan_hebat/pages/data_input/input_piezometer.dart';
import 'package:bendungan_hebat/pages/data_input/input_3dag.dart';
import 'package:bendungan_hebat/pages/data_input/input_ringmagnet.dart';
import 'package:bendungan_hebat/pages/data_input/input_tailrace.dart';
import 'package:bendungan_hebat/pages/data_input/input_tmawaduk.dart';
import 'package:bendungan_hebat/pages/data_input/input_v-notch.dart';
import 'package:bendungan_hebat/pages/homepage/views/3d_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/3dt_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/ich_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/leak_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/ow_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/p_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/ringm_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/t_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/tmaw_view.dart';
import 'package:bendungan_hebat/pages/homepage/views/vn_view.dart';
import 'package:bendungan_hebat/services/damDataServices.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.openIndex, required this.title})
      : super(key: key);
  final int openIndex;
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController pageController = new PageController();
  RefreshDataController rControl = Get.put(RefreshDataController());
  List<DataOW> dataOw = [];
  List<DataPiezometer> dataPiezo = [];
  List<DataVNotch> dataVNotch = [];
  List<Data3D> data3D = [];
  List<Data3DT> data3DTower = [];
  List<DataLeakage> dataLeakage = [];
  List<DataTailrace> dataTailrace = [];
  List<DataIch> dataICH = [];
  List<DataTmaw> dataTMAW = [];
  List<Data3DT> data3DT = [];
  List<DataRingMagnet> dataRM = [];

  int dataLength = 0;
  bool isLoading = true;

  readData() async {
    try {
      switch (widget.openIndex) {
        case 0:
          DamDataServices().getOWData().then((value) {
            setState(() {
              dataOw = value;
              dataLength = dataOw.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 1:
          DamDataServices().getPiezoData().then((value) {
            setState(() {
              dataPiezo = value;
              dataLength = dataPiezo.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 2:
          DamDataServices().get3DGalleryData().then((value) {
            setState(() {
              data3D = value;
              dataLength = data3D.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 3:
          DamDataServices().get3DTowerData().then((value) {
            setState(() {
              data3DT = value;
              dataLength = data3DT.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 4:
          DamDataServices().getVNotchdata().then((value) {
            setState(() {
              dataVNotch = value;
              dataLength = dataVNotch.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 5:
          DamDataServices().getRingMData().then((value) {
            setState(() {
              dataRM = value;
              dataLength = dataRM.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 6:
          DamDataServices().getLeakData().then((value) {
            setState(() {
              dataLeakage = value;
              dataLength = dataLeakage.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 7:
          DamDataServices().getTMAWData().then((value) {
            setState(() {
              dataTMAW = value;
              dataLength = dataTMAW.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 8:
          DamDataServices().getIntesitasCurahHujan().then((value) {
            setState(() {
              dataICH = value;
              dataLength = dataICH.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        case 9:
          DamDataServices().getTailraceData().then((value) {
            setState(() {
              dataTailrace = value;
              dataLength = dataTailrace.length;
              isLoading = false;
              rControl.refreshData(false);
            });
          });
          break;
        default:
          return;
      }
    } catch (e) {}
  }

  @override
  void initState() {
    if (mounted) {
      ever(rControl.updating, (bool f) {
        if (f) {
          setState(() {
            isLoading = true;
          });
          readData();
        }
      });
      readData();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget floatingBtn() => InkWell(
          borderRadius: BorderRadius.circular(100.0),
          onTap: () {
            switch (widget.openIndex) {
              case 0:
                Get.to(InputOW())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 1:
                Get.to(InputPiezometer())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 2:
                Get.to(Input3Dimension())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 3:
                Get.to(Input3DimensionT())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 4:
                Get.to(InputVNotch())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 5:
                Get.to(InputRingMagnet())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 6:
                Get.to(InputLeakage())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 7:
                Get.to(InputTMAWaduk())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 8:
                Get.to(InputCurahHujan())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              case 9:
                Get.to(InputTailrace())!.then((value) {
                  print("refresh data");
                  readData();
                });
                break;
              default:
                return;
            }
          },
          child: Container(
            alignment: Alignment.center,
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Theme.of(context).accentColor,
              gradient: LinearGradient(
                begin: Alignment(-0.2, 1.5),
                end: Alignment.topRight,
                stops: [0.0, 1.0],
                colors: [
                  ConfigTheme.secondColor,
                  ConfigTheme.thirdColor,
                ],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(
                  Icons.add,
                  size: 38,
                  color: Colors.white,
                ),
                Text(
                  "Tambah", //TODO: LOCALIZE
                  style: TextStyle(color: Colors.white, fontSize: 11.0),
                )
              ],
            ),
          ),
        );

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: floatingBtn(),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          SizedBox(height: 45.0),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(width: 10.0),
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(Icons.arrow_back),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      widget.title,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.search,
                  ),
                ),
                SizedBox(width: 10.0),
              ],
            ),
          ),
          Expanded(
            child: (isLoading)
                ? Container(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(),
                  )
                : Container(
                    child: ListView.builder(
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.only(top: 10.0),
                      itemCount: dataLength,
                      itemBuilder: (ctx, i) {
                        switch (widget.openIndex) {
                          case 0:
                            return OWView(sData: dataOw[i]);
                          case 1:
                            return PiezoView(sData: dataPiezo[i]);
                          case 2:
                            return ThreeDView(sData: data3D[i]);
                          case 3:
                            return ThreeDTowerView(sData: data3DT[i]);
                          case 4:
                            return VNotchView(sData: dataVNotch[i]);
                          case 5:
                            return RingMView(sData: dataRM[i]);
                          case 6:
                            return LeakView(sData: dataLeakage[i]);
                          case 7:
                            return TMAWadukView(sData: dataTMAW[i]);
                          case 8:
                            return ICHView(sData: dataICH[i]);
                          case 9:
                            return TailraceView(sData: dataTailrace[i]);
                          default:
                            return Container(
                                alignment: Alignment.center,
                                child: Text(
                                    "Terjadi Kesalahan, harap muat ulang halaman"));
                        }
                      },
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}

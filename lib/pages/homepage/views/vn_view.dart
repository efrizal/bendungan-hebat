import 'dart:io';

import 'package:bendungan_hebat/model/VNotchData.dart';
import 'package:bendungan_hebat/pages/data_input/input_v-notch.dart';
import 'package:bendungan_hebat/services/InputServices.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_compression/image_compression.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class VNotchView extends StatefulWidget {
  const VNotchView({Key? key, required this.sData}) : super(key: key);
  final DataVNotch sData;

  @override
  _VNotchViewState createState() => _VNotchViewState();
}

class _VNotchViewState extends State<VNotchView> {
  final picker = ImagePicker();
  File? _image;
  String? imgUrl;

  Future getImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.camera);

    if (pickedFile != null) {
      _image = File(pickedFile.path);

      final input = ImageFile(
        rawBytes: _image!.readAsBytesSync(),
        filePath: _image!.path,
      );

      final output = compress(
        ImageFileConfiguration(
          input: input,
          config: Configuration(
            pngCompression: PngCompression.bestCompression,
            outputType: OutputType.jpg,
            jpgQuality: 30,
          ),
        ),
      );

      final buffer = output.rawBytes.buffer;
      var fileout = await File(_image!.path).writeAsBytes(buffer.asUint8List(
          output.rawBytes.offsetInBytes, output.rawBytes.lengthInBytes));
      final String path =
          await getApplicationDocumentsDirectory().then((value) => value.path);
      final File newImage = await fileout.copy(
          '$path/${widget.sData.tanggal}-${widget.sData.petugas}${widget.sData.id}${widget.sData.perangkatId}.jpg');
      _image = newImage;

      String netPath =
          "piezo/${widget.sData.tanggal}-${widget.sData.petugas}${widget.sData.id}${widget.sData.perangkatId}";

      List<String> netField = [
        widget.sData.id.toString(),
        widget.sData.deviceTypeId.toString()
      ];

      InputServices().upImgHttp(netPath, _image!, netField).then((value) {
        setState(() {
          imgUrl = value;
        });
      });
    } else {
      print('No image selected.');
    }
  }

  @override
  void initState() {
    setState(() {
      if ((widget.sData.foto == null)) {
        imgUrl = null;
      } else {
        imgUrl = widget.sData.foto;
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var data = widget.sData;

    return Container(
      height: 210.0,
      padding: EdgeInsets.all(10.0),
      margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: [
          BoxShadow(
            blurRadius: 5.0,
            color: Colors.black12,
            offset: Offset(0.0, 0.0),
            spreadRadius: 1.0,
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            data.tanggal! + " - ${data.namaPerangkat}",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 10.0),
          Expanded(
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: (data.foto == null)
                      ? Image.asset(
                          "assets/images/no-image.jpeg",
                          width: 120.0,
                          height: 150.0,
                          fit: BoxFit.cover,
                        )
                      : Image.network(
                          data.foto!,
                          width: 120.0,
                          height: 150.0,
                          fit: BoxFit.cover,
                          errorBuilder: (ctx, o, s) {
                            return Image.asset(
                              "assets/images/no-image.jpeg",
                              width: 120.0,
                              height: 150.0,
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                ),
                SizedBox(width: 10.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Petugas",
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      widget.sData.petugas!,
                      style: TextStyle(
                        fontSize: 13.0,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(height: 3.0),
                    Text(
                      "Tipe V-Notch : ",
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      "${data.vnotchType}",
                      style: TextStyle(
                        fontSize: 13.0,
                        // fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(height: 3.0),
                    Text(
                      "Level TMA V-Notch : ",
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      "${data.nilaiPengukuran}",
                      style: TextStyle(
                        fontSize: 13.0,
                        // fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(height: 3.0),
                    Text(
                      "Nilai Debit (Q) : ",
                      style: TextStyle(
                        fontSize: 13.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    Text(
                      "${data.nilaiPengolahan}",
                      style: TextStyle(
                        fontSize: 13.0,
                        // fontWeight: FontWeight.bold,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(height: 5.0),
                    Container(
                      width: Get.width * 0.5,
                      child: Row(
                        // mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Expanded(child: Container()),
                          GestureDetector(
                            onTap: () {
                              getImage();
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 10.0),
                              decoration: BoxDecoration(
                                color: ConfigTheme.thirdColor,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5.0),
                                  bottomLeft: Radius.circular(5.0),
                                ),
                              ),
                              child: Icon(
                                Icons.upload_file,
                                size: 16.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          SizedBox(width: 1.0),
                          GestureDetector(
                            onTap: () {
                              Get.to(InputVNotch(editData: data, isEdit: true));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: ConfigTheme.thirdColor,
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(5.0),
                                  bottomRight: Radius.circular(5.0),
                                ),
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 10.0),
                              child: Icon(
                                Icons.edit_rounded,
                                size: 16.0,
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'dart:async';

import 'package:bendungan_hebat/pages/homepage/homepage.dart';
import 'package:bendungan_hebat/services/authServices.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import 'index_page.dart';

class SignInEmail extends StatefulWidget {
  static const String routeName = '/material/text-form-field';

  @override
  SignInEmailState createState() => new SignInEmailState();
}

class PersonData {
  String email = '';
  String password = '';
}

class LogsResponse {
  String code = '';
  String status = '';
  String message = '';
}

class SignInEmailState extends State<SignInEmail> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<FormFieldState<String>> _passwordFieldKey =
      new GlobalKey<FormFieldState<String>>();
  bool startSignIn = false;

  PersonData person = new PersonData();
  bool _obscureText = false;

  bool _autovalidate = false;
  bool _formWasEdited = false;

  final storage = new FlutterSecureStorage();

  late String email, password;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void showInSnackBar(String value) {
    ScaffoldMessenger.of(context).showSnackBar(new SnackBar(
      duration: const Duration(milliseconds: 1000),
      content: new Container(
        padding: EdgeInsets.all(2.0),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Icon(Icons.info, color: Theme.of(context).accentColor),
            const SizedBox(width: 10.0),
            new Flexible(
              child: new Text(value),
            )
          ],
        ),
      ),
    ));
  }

  void _handleSubmitted() async {
    final FormState? form = _formKey.currentState;
    FocusScope.of(context).unfocus();
    if (!form!.validate()) {
      _autovalidate = true; // Start validating on every change.
      showInSnackBar("Harap Masukkan Data Dengan Benar");
    } else {
      form.save();
      showInSnackBar("Mencoba Masuk");
      setState(() {
        startSignIn = true;
      });
      try {
        //TODO: DO LOGIN HERE!
        await storage.write(key: "password", value: password);

        AuthServices().signIn(email: email, password: password).then((value) {
          showInSnackBar("Berhasil Masuk, mengalihkan");
          Timer(Duration(seconds: 2), () {
            var user = value;
            Get.off(IndexPage());
          });
        }).onError((error, stackTrace) {
          print(error.toString());
          setState(() {
            startSignIn = false;
          });
          showInSnackBar("Data yang anda masukkan tidak benar");
        });
      } catch (e) {
        showInSnackBar("Terjadi kesalahan, harap coba lagi beberapa saat lagi");
        setState(() {
          startSignIn = false;
        });
        print("CATCH : " + e.toString());
      }
    }
  }

  String? _validatePassword(value) {
    _formWasEdited = true;
    final FormFieldState<String>? passwordField =
        _passwordFieldKey.currentState;
    if (passwordField!.value == null || passwordField.value!.isEmpty)
      return 'Please enter a password.';
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      body: Container(
        height: Get.height,
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                padding: EdgeInsets.all(20.0),
                width: MediaQuery.of(context).size.width,
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 30.0),
                      Hero(
                        tag: "img1",
                        child: Image.asset(
                          "assets/images/main-logo.png",
                          width: MediaQuery.of(context).size.width * 0.3,
                        ),
                      ),
                      SizedBox(height: 50.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Masuk untuk melanjutkan",
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                          SizedBox(height: 10.0),
                          TextFormField(
                            validator: (value) {
                              Pattern pattern =
                                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                              RegExp regex = new RegExp(pattern.toString());

                              if (value!.isNotEmpty && !regex.hasMatch(value))
                                return 'E-mail anda tidak valid';
                              else if (value.isEmpty) {
                                return 'Harap masukkan E-mail anda';
                              } else
                                return null;
                            },
                            keyboardType: TextInputType.emailAddress,
                            maxLines: 1,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.grey[200],
                              hintText: "E-mail",
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 0.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                borderSide: BorderSide(
                                    color: Colors.grey[200]!, width: 1.5),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                borderSide: BorderSide(
                                    color: Colors.grey[200]!, width: 1.5),
                              ),
                            ),
                            onChanged: (val) {
                              setState(() {
                                email = val;
                              });
                            },
                            onSaved: (val) {
                              setState(() {
                                email = val!;
                              });
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 15.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextFormField(
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Harap masukkan password anda';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.visiblePassword,
                            obscureText: !_obscureText,
                            maxLines: 1,
                            decoration: InputDecoration(
                              suffixIcon: IconButton(
                                  icon: Icon((_obscureText)
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                  onPressed: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  }),
                              filled: true,
                              fillColor: Colors.grey[200],
                              hintText: "Password",
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 0.0),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                borderSide: BorderSide(
                                    color: Colors.grey[200]!, width: 1.5),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                borderSide: BorderSide(
                                    color: Colors.grey[200]!, width: 1.5),
                              ),
                            ),
                            onChanged: (val) {
                              setState(() {
                                password = val;
                              });
                            },
                            onSaved: (val) {
                              setState(() {
                                password = val!;
                              });
                            },
                          ),
                          SizedBox(height: 10.0),
                          Container(
                            alignment: Alignment.centerRight,
                            child: GestureDetector(
                              onTap: () {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(
                                        'Harap Hubungi Admin untuk memperbarui password akun!'),
                                    duration: Duration(seconds: 1),
                                  ),
                                );
                              },
                              child: Text(
                                "Lupa Password ?  Klik disini  ",
                                style: TextStyle(
                                    color: ConfigTheme.secondColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12.0),
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 30.0),
                      ElevatedButton(
                        onPressed: () {
                          _handleSubmitted();
                        },
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  (startSignIn) ? 50 : 10.0)),
                          primary: Theme.of(context).accentColor,
                        ),
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 500),
                          curve: Curves.easeInExpo,
                          width: (startSignIn) ? 50 : Get.width - 80.0,
                          height: (startSignIn) ? 50 : 50,
                          padding: EdgeInsets.all(15.0),
                          alignment: Alignment.center,
                          child: (startSignIn)
                              ? Container(
                                  width: 20.0,
                                  height: 20.0,
                                  child: CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  ),
                                )
                              : Text(
                                  "Masuk",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                        ),
                      ),
                      SizedBox(height: 15.0),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: 'Belum punya akun?',
                          style: TextStyle(color: Colors.grey, fontSize: 14.0),
                          children: <TextSpan>[
                            TextSpan(
                              text: ' Daftar Disini',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: ConfigTheme.secondColor),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          'Hubungi Admin untuk membuat akun!'),
                                      duration: Duration(seconds: 1),
                                    ),
                                  );
                                },
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 30.0),
                      // Row(
                      //   children: <Widget>[
                      //     Expanded(
                      //       child: Container(
                      //         color: Colors.grey[300],
                      //         height: 1.0,
                      //       ),
                      //     ),
                      //     SizedBox(width: 10.0),
                      //     Text(
                      //       "Atau Masuk Menggunakan",
                      //       style: TextStyle(
                      //           color: Colors.grey[400], fontSize: 10.0),
                      //     ),
                      //     SizedBox(width: 10.0),
                      //     Expanded(
                      //       child: Container(
                      //         color: Colors.grey[300],
                      //         height: 1.0,
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(height: 30.0),
                      // Row(
                      //   children: <Widget>[
                      //     Expanded(
                      //       child: RaisedButton(
                      //         onPressed: () {
                      //           // doGoogleSignIn(context);
                      //         },
                      //         shape: RoundedRectangleBorder(
                      //             borderRadius: BorderRadius.circular(10.0)),
                      //         child: Container(
                      //           height: 50.0,
                      //           alignment: Alignment.center,
                      //           child: Row(
                      //             mainAxisAlignment: MainAxisAlignment.center,
                      //             children: <Widget>[
                      //               Icon(
                      //                 MdiIcons.google,
                      //                 color: Colors.white,
                      //               ),
                      //               SizedBox(width: 10.0),
                      //               Text(
                      //                 "Google",
                      //                 style: TextStyle(
                      //                     color: Colors.white,
                      //                     fontWeight: FontWeight.bold),
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //         color: Color(0xffef403c),
                      //       ),
                      //     ),
                      //     SizedBox(width: 10.0),
                      //     Expanded(
                      //       child: RaisedButton(
                      //         onPressed: () {
                      //           // doFacebookSignIn(context);
                      //         },
                      //         shape: RoundedRectangleBorder(
                      //             borderRadius: BorderRadius.circular(10.0)),
                      //         child: Container(
                      //           height: 50.0,
                      //           alignment: Alignment.center,
                      //           child: Row(
                      //             mainAxisAlignment: MainAxisAlignment.center,
                      //             children: <Widget>[
                      //               Icon(
                      //                 MdiIcons.facebook,
                      //                 color: Colors.white,
                      //               ),
                      //               SizedBox(width: 10.0),
                      //               Text(
                      //                 "Facebook",
                      //                 style: TextStyle(
                      //                     color: Colors.white,
                      //                     fontWeight: FontWeight.bold),
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //         color: Color(0xff3b5998),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      SizedBox(height: 50.0),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0.0,
              child: Container(
                height: 3.0,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        color: ConfigTheme.thirdColor,
                        height: 5.0,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.2,
                      color: ConfigTheme.secondColor,
                      height: 5.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:async';

import 'package:bendungan_hebat/model/LeakageData.dart';
import 'package:bendungan_hebat/model/OWData.dart';
import 'package:bendungan_hebat/model/PiezometerData.dart';
import 'package:bendungan_hebat/model/ThreeDimensionData.dart';
import 'package:bendungan_hebat/model/VNotchData.dart';
import 'package:bendungan_hebat/pages/homepage/homepage.dart';
import 'package:bendungan_hebat/pages/index_page.dart';
import 'package:bendungan_hebat/pages/sign_in.dart';
import 'package:bendungan_hebat/utils/config.dart';
import 'package:bendungan_hebat/utils/configTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isAppActive = true;

  startApps() async {
    if (isAppActive) {
      try {
        await setupHive();
        // await Config().setupRemoteConfig();
        Timer(Duration(seconds: 2), () async {
          var prefs = await SharedPreferences.getInstance();
          var value = (prefs.getBool("islogin") == null)
              ? false
              : prefs.getBool("islogin");

          if (value!) {
            Get.off(IndexPage());
          } else {
            Get.off(SignInEmail());
          }
        });
      } catch (e) {
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              children: [
                Container(
                  padding: EdgeInsets.only(
                    left: 15.0,
                    right: 15.0,
                    top: 20.0,
                    bottom: 0.0,
                  ),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.cancel_outlined,
                        size: 100.0,
                        color: ConfigTheme.secondColor,
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        "Error!",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        e.toString(),
                        style: TextStyle(
                          fontSize: 13.0,
                        ),
                      ),
                      SizedBox(height: 15.0),
                      ElevatedButton(
                        onPressed: () {
                          Clipboard.setData(ClipboardData(text: e.toString()));
                        },
                        style: ElevatedButton.styleFrom(
                          primary: ConfigTheme.secondColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                        ),
                        child: Text("Copy ke Clipboard"),
                      )
                    ],
                  ),
                )
              ],
            );
          },
        );
      }
    }
  }

  setupHive() async {
    try {
      final appDocumentDir = await getApplicationDocumentsDirectory();
      Hive.init(appDocumentDir.path);
      // Hive.registerAdapter(DataOWAdapter());
      // Hive.registerAdapter(DataPiezometerAdapter());
      // Hive.registerAdapter(DataVNotchAdapter());
      // Hive.registerAdapter(Data3DAdapter());
      // Hive.registerAdapter(DataLeakageAdapter());
      // await Hive.openBox('owData');
      // await Hive.openBox('piezoData');
      // await Hive.openBox('vNotchData');
      // await Hive.openBox('3DData');
      await Hive.openBox('remoteConfig');
    } catch (e) {
      print(e.toString());
      return throw e;
    }
  }

  @override
  void initState() {
    if (mounted) {
      startApps();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Get.width,
        height: Get.height,
        color: Colors.white,
        alignment: Alignment.center,
        child: Column(
          children: [
            Expanded(child: Container()),
            Hero(
              tag: "img1",
              child: Image.asset(
                "assets/images/main-logo.png",
                width: Get.width * 0.4,
              ),
            ),
            Expanded(child: Container()),
            Text(
              "App Version 0.0.1",
              style: TextStyle(
                color: Colors.grey[400],
              ),
            ),
            SizedBox(height: Get.height * 0.075),
          ],
        ),
      ),
    );
  }
}
